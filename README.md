[![Build Status](https://travis-ci.com/julek14/Quay.svg?token=xreXjtTKfpJG4gzUDcZs&branch=master)](https://travis-ci.com/julek14/Quay)
# Quay
3rd year Major Group Project 

The name Quay (**QUA**lit**Y** as in Code Quality App) was generted by [an acronym generator](http://acronymcreator.net/).

Available at **[quay-app.com](http://www.quay-app.com)** and **[quayapp.com](http://www.quayapp.com)**.

## About
Quay is a web application that allows users to perform code validation. Validation means ensuring that the code syntax
is valid and that it adheres to some accepted standard. 

Validation is performed by third-party APIs. Quay is a wrapper around those APIs. It accepts code to validate, sends 
validation requests to the APIs, formats the API response, and sends back a validation report.

Currently, HTML and CSS validation is available. They are validated using the 
[W3 HTML validation api](https://validator.w3.org/docs/api.html) and the 
[W3 CSS validation API](https://jigsaw.w3.org/css-validator/api.html).

The application provides a REST API through which code can be submitted for validation.

It allows validating GitHub repositories. After registering with your GitHub account, you can go on the website 
and select the repo you want to validate. 

Quay is also available as a GitHub integration, so you can install it on a repository and every time a 'git push' event 
is detected, Quay will validate your code and leave a status on the commit that triggered the event. The status includes a
link to the validation report for the particular commit. This report is only visible to the user who has a Quay account 
and installed the integration on the repo. 

## REST API
The application backend can be used as a REST API. This section describes how to make requests to the API.

#### Sending Code
To validate a string of code

> Send the request to **/v/text**

Here is a cURL example:

> curl -X POST -H "Content-Type: application/json"
> -d '{"text":"<!DOCTYPE html><html>your html code here</html>",
> "lang": "html"}' "http://www.quay-app.com/v/text"

#### Sending a File
To validate a file

> Send the request to **/v/file**

Here is a cURL example:

> curl -X POST -H "Content-Type: multipart/form-data"
> -F "file=@C:\Users\julek14\Documents\HTML\file.html" 
> -F "lang=html" "http://www.quay-app.com/v/file"

#### Sending a Zip
To validate an entire zipped project

> Send the request to **/v/zip**

Here is a cURL example:

> curl -X POST -H "Content-Type: multipart/form-data"
> -F "zip=@C:\Users\julek14\Documents\zip\project.zip" 
> "http://www.quay-app.com/v/zip"

**Note:** At the moment you cannot perform GitHub repository validation through the API. That must be either done 
through the website or installing Quay as a GitHub integration.

## Other notes
To build documentation for this project, download it and run:
> npm run documentation

To run unit tests without a coverage report, run:
> npm run test-simple

To run unit tests and generate a coverage report, run:
> num run test-coverage

This application is hosted on Heroku and uses Heroku environment variables for configuration. These are hidden from Git.
The application will not run without them.


*This project was created by [me](https://github.com/julek14) and [Mateusz](https://github.com/MatNit).*
