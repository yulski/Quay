const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const fileStreamRotator = require('file-stream-rotator');
const fs = require('fs');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const compression = require('compression');
const session = require('express-session');
const flash = require('express-flash');
const passport = require('passport');
const GithubStrategy = require('passport-github2').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const redis = require('redis');

const index = require('./routes/index');
const v = require('./routes/v');
const user = require('./routes/user');
const gh = require('./routes/gh');

const User = require('./models').User;

// services
const GitHubService = require('./services/gh');
const MedalService = require('./services/medals');
const RandstrService = require('./services/randstr');
const ReportService = require('./services/reports');
const UploadService = require('./services/upload');
const ValidationService = require('./services/validation');

const app = express();

app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));

app.use(compression());

const logDirectory = __dirname + '/log';

fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

const accessLogStream = fileStreamRotator.getStream({
	date_format: 'YYYYMMDD',
	filename: logDirectory + '/access-%DATE%.log',
	frequency: 'daily',
	verbose: false
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

// redis
const REDIS_CONNECTION_STRING = process.env.REDIS_CONNECTION_STRING;
const redisClient = redis.createClient({
    url: REDIS_CONNECTION_STRING
});
app.set('redis', redisClient);

app.use(logger('dev', {
	stream: accessLogStream
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
	secret: 'secret', // TODO: change this
	saveUninitialized: true,
	resave: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(expressValidator({
	customValidators: {
		isValidPassword: (password) => {
			let regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/g;
			return password.length > 7 && password.length < 99 && regex.test(password);
		}
	},
	errorFormatter: (param, msg, value) => {
		let namespace = param.split('.'),
			root = namespace.shift(),
			formParam = root;

		while (namespace.length) {
			formParam += '[' + namespace.shift() + ']';
		}

		return {
			param: formParam,
			msg: msg,
			value: value
		}
	}
}));

app.use(flash());

// inject services
let githubService = new GitHubService();
let medalService = new MedalService();
let randstrService = new RandstrService();
let reportService = new ReportService(medalService);
let uploadService = new UploadService();
let validationService = new ValidationService(medalService, githubService, reportService, uploadService);

app.set('gh', githubService);
app.set('medals', medalService);
app.set('randstr', randstrService);
app.set('reports', reportService);
app.set('upload', uploadService);
app.set('validation', validationService);

passport.use('login', new LocalStrategy({
        // use email instead of username
        usernameField: 'email'
    },
    (email, password, done) => {
        // find a user with matching email
        User.findOne({
            where: {
                email: email
            }
        }).then((user) => {
            if (user) {
                // if found, compare passwords
                User.comparePassword(password, user.password).then((match) => {
                    // return with result
                    return done(null, match ? user : false);
                }).catch((err) => {
                    throw err;
                });
            } else {
                // return false if not found
                return done(null, false);
            }
        }).catch((err) => {
            throw err;
        });
    }
));

passport.use('github', new GithubStrategy({
    clientID: process.env.GH_CLIENT_ID,
    clientSecret: process.env.GH_CLIENT_SECRET,
    callbackUrl: 'http://localhost:5000/user/gh-callback',
    scope: 'user',
    passReqToCallback: true
}, (req, accessToken, refreshToken, profile, done) => {
    // if a user is logged in, that means they are trying to link their github account
    // so make sure the github account email matches the email the user is registered with
    if(req.user && (req.user.email !== profile.emails[0].value)) {
        return done(null, false);
    }
    User.findOrCreate({
        where: {
            email: profile.emails[0].value,
        }
    }).spread((user, created) => {
        // update user profile with GitHub credentials
        let userOptions = {
            full: true,
            ghid: profile.id,
            ghtoken: accessToken,
            ghuname: profile.username
        };
        user.update(userOptions).then(() => {
            return done(null, user);
        });
    });
}));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id).then((user) => {
        done(null, user);
    }).catch((err) => {
        done(err);
    });
});

// routes
app.use('/', index);
app.use('/v', v);
app.use('/user', user);
app.use('/gh', gh);

// catch 404 and forward to error handler
app.use((req, res, next) => {
	let err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use((err, req, res, next) => {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
