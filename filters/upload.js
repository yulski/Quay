/**
 * Filters for handling file uploads. Filters certain files and saves 
 * the accepted files to a directory.
 * @module upload
 */

const path = require('path');
const multer = require('multer');

const fh = require('../src/file-handling');

/**
 * Directory that uploaded files will be saved to.
 */
const outDir = path.join(__dirname, '..', 'temp');

/**
 * Multer config to save files to outDir and generate a filename for the
 * uploaded files.
 */
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, outDir);
    },
    filename: (req, file, cb) => {
        let name = generateFileName(file.originalname);
        cb(null, name);
    }
});

/**
 * Generate file name for uploaded files.
 * @param {string} fileName - Original name of file.
 * @returns {string} The name the file should be saved with.
 */
function generateFileName (fileName) {
    let arr = fileName.split('.');
    let ext = arr.pop();
    // name should be in the form [original name]-[timestamp].[extension]
    let name = arr.join('.') + '-' + Date.now() + '.' + ext;
    return name;
}

/**
 * Filters uploaded files.
 * @param {Request} req - HTTP request object
 * @param {File} file - the multer uploaded file 
 * @param {Function} cb - multer callback
 */
function fileFilter (req, file, cb) {
    let res = fh.checkFileExtension(file.originalname);
    cb(null, res);
}

/**
 * Filters uploaded zips.
 * @param {Request} req - HTTP request object
 * @param {File} file - the multer uploaded file 
 * @param {Function} cb - multer callback
 */
function zipFilter (req, file, cb) {
    let res = fh.checkArchiveExtension(file.originalname);
    cb(null, res);
}

/**
 * Multer file upload.
 */
const fileUpload = multer({
    storage: storage,
    fileFilter: fileFilter
});

/**
 * Multer zip upload.
 */
const zipUpload = multer({
    storage: storage,
    fileFilter: zipFilter
});

module.exports = {
    outDir: outDir,
    fileUpload: fileUpload,
    zipUpload: zipUpload
}

