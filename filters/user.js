/**
 * Filters to deny users access to certain routes based on login status. 
 * @module user
 */

/**
 * If the user is logged in, continue with the request. Otherwise, send user to
 * error page.
 */
function ensureLoggedIn () {
    return (req, res, next) => {
        if (req.user && req.isAuthenticated()) {
            next();
        } else {
            res.status(400).render('error', {
                message: 'You are not logged in.',
                statusCode: 400
            });
        }
    };
}

/**
 * If the user is not logged in, continue with the request. Otherwise send user to 
 * error page.
 */
function ensureNotLoggedIn () {
    return (req, res, next) => {
        if (req.user && req.isAuthenticated()) {
            res.status(400).render('error', {
                message: 'You are already logged in.',
                statusCode: 400
            });
        } else {
            next();
        }
    };
}

module.exports = {
    ensureLoggedIn: ensureLoggedIn,
    ensureNotLoggedIn: ensureNotLoggedIn
};

