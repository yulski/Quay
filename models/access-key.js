/**
 * AccessKey entity. Used for email verification and password reset keys.
 */
module.exports = (sequelize, DataTypes) => {
    const AccessKey = sequelize.define('AccessKey', {
        hash: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        },
        user: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                isInt: true
            }
        },
        created: {
            type: DataTypes.DATE,
            allowNull: false,
            validate: {
                isDate: true
            }
        },
        expires: {
            type: DataTypes.DATE,
            allowNull: false,
            validate: {
                isDate: true
            }
        },
        type: {
            type: DataTypes.ENUM,
            allowNull: false,
            values: ['password_reset', 'email_verification']
        }
    }, {
        timestamps: false,
        tableName: 'access_keys'
    });

    return AccessKey;
};