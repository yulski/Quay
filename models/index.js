const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

// get database config from env
const DB_ARR = process.env.JAWSDB_URL.split(':');
const DB_HOST = DB_ARR[2].split('@')[1];
const DB_USER = DB_ARR[1].substr(2);
const DB_PASS = DB_ARR[2].split('@')[0];
const DB_NAME = DB_ARR.pop().split('/').pop();

// create and configure sequelize
const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
	host: DB_HOST,
	dialect: 'mysql',
    // pool connections
	pool: {
		max: 5,
		min: 0,
		idle: 10000
	}
});

const db = {};

// read in models
fs.readdirSync(__dirname)
    .filter((file) => {
        return (file.indexOf('.') !== 0) && (file !== 'index.js');
    })
    // import all models 
    .forEach((file) => {
        let model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
