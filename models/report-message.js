/**
 * Entity to associate a message with a report.
 */
module.exports = (sequelize, DataTypes) => {
    // represents database 'report_messages' table
    const ReportMessage = sequelize.define('ReportMessage', {
        report: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        message: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, {
        timestamps: false,
        tableName: 'report_messages'
    });

    return ReportMessage;
};
