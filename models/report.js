/**
 * A validation report. 
 */
module.exports = (sequelize, DataTypes) => {
    // represents database 'reports' table
    const Report = sequelize.define('Report', {
        date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        user: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        medal: {
            type: DataTypes.ENUM,
            allowNull: false,
            values: ['GOLD', 'SILVER', 'BRONZE']
        }
    }, {
        timestamps: false,
        tableName: 'reports'
    });

    return Report;
};
