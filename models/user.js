const bcrypt = require('bcrypt');
const saltRounds = +process.env.SALT_ROUNDS || 12;

/**
 * User entity.
 */
module.exports = (sequelize, DataTypes) => {
    // represents database 'user' table
    const User = sequelize.define('User', {
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isEmail: true,
                notEmpty: true
            }
        },
        password: {
            type: DataTypes.STRING,
            allowNull: true
        },
        full: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
            defaultValue: false
        },
        ghid: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: null
        },
        ghtoken: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: null
        },
        ghuname: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: null
        }
    }, {
        classMethods: {
            comparePassword: (password, hash) => {
                return bcrypt.compare(password, hash);
            }
        },
        timestamps: false,
        tableName: 'users'
    });

    return User;
};
