// standard timeout length for showing notifications or messages
var timeoutLength = 5000;

// reset footer to original position
function resetFooter() {
    var footer = $('footer')[0];
    $($('body')[0]).position('relative');
    if($(footer).hasClass('stick-to-bottom')) {
        $(footer).removeClass('stick-to-bottom');
    }
}

// adjust footer position
function adjustFooter() {
    var footer = $('footer')[0];
    var windowHeight = $(window).height();
    var documentHeight = $(document).height();
    
    if(documentHeight === windowHeight && !$(footer).hasClass('stick-to-bottom')) {
        $(footer).addClass('stick-to-bottom');
        $($('body')[0]).css('position', 'static');
    } else if(documentHeight > windowHeight && $(footer).hasClass('stick-to-bottom')) {
        $(footer).removeClass('stick-to-bottom');
        $($('body')[0]).css('position', 'relative');
    }
}

function showMessage(header, body, style) {
    var msg = $('#message');
    $(msg)[0].className = 'message is-' + style;
    $('#message-header').html(header);
    $('#message-body').html(body);
    $(msg).show();
}

(function() {
    adjustFooter();
    $(window).resize(adjustFooter);
    $(document).on('resize', adjustFooter);
})();
