var allowFormSubmission = true;

function handleFormSubmission(evt) {
    if (!allowFormSubmission) {
        return false;
    }
    allowFormSubmission = false;
    var email = $('#email').val();
    var options = {
        data: {email: email},
        error: function(jqxhr, textStatus, error) {
            var msg =JSON.parse(jqxhr.responseText).errors.msg;
            showMessage('Error', msg, 'danger');
            allowFormSubmission = true;
        },
        method: 'POST',
        success: function(data, statusText, jqxhr) {
            var message = data.message;
            showMessage('Success', message, 'info');
            allowFormSubmission = true;
        },
        url: ''
    };
    jQuery.ajax(options);
    return false;
}

(function() {
    $('#form').submit(handleFormSubmission);
})();
