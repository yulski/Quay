// --------------------------------------------------------------------
// ------------------------- global variables -------------------------
// --------------------------------------------------------------------
// editor for user input
var editor;
// objects detailing highlighted lines
var highlights = [];
// maximum allowed file size (approx. 100MB)
var fileMaxSize = 100000000;
var allowValidationSubmission = true;
var allowLoginSubmission = true;
var allowLogoutSubmission = true;
var allowRegisterSubmission = true;

// ---------------------------------------------------------------------------------------
// ------------------------- constructors for ACE editor objects -------------------------
// ---------------------------------------------------------------------------------------
// ace Range contructor
var Range = ace.require('ace/range').Range;
// ace Anchor constructor
var Anchor = ace.require('ace/anchor').Anchor;

// -------------------------------------------------------------
// ------------------------- functions -------------------------
// -------------------------------------------------------------

// checks if the file selected for upload is not too large
function checkFileSize() {
    var inputType = $('#input-method').val();
    var file = $('#' + inputType).prop('files')[0];
    return file.size < fileMaxSize;
}

// removes all error highlighting in the editor
function clearAllHighlights() {
    var len = highlights.length;
    for (var i = len - 1; i >= 0; i--) {
        clearHighlights(i);
    }
}

// clears a single highlight in the editor
function clearHighlights(index) {
    // detach the anchor
    highlights[index].anchor.detach();
    // remove the range
    highlights[index].range = null;
    // remove the marker from the editor
    editor.getSession().removeMarker(highlights[index].marker);
    // remove the object from the highlights array
    highlights.splice(index, 1);
}

// get the content that is being sent for validation
function getInputContent() {
    // first get the input method
    var inputType = $('#input-method').val();
    var content = null;
    // get the appropriate content for the selected input method
    switch (inputType) {
        case 'text':
            // if text, content is an object with the editor 
            // text under the key 'text'
            content = {};
            content.text = editor.getValue();
            // return null if text is empty
            if (content.text.trim().length === 0) {
                return null;
            }
            break;
        case 'file':
            // if file, get the file that is to be uploaded and
            // add it to a FormData object under the name 'file'
            var file = $('#file').prop('files')[0];
            // if no file selected, return null
            if (!file) {
                return null;
            }
            content = new FormData();
            content.append('file', file);
            break;
        case 'zip':
            // if zip, get the zip that is to be uploaded and
            // add it to a FormData object under the name 'zip'
            var zip = $('#zip').prop('files')[0];
            // if no zip, return null
            if (!zip) {
                return null;
            }
            content = new FormData();
            content.append('zip', zip);
            break;
        case 'repo':
            // if repo, content is an object with the repo name
            // under the key 'repoName'
            var repoName = $('#repo').val();
            content = {};
            content.repoName = repoName;
            break;
    }
    return content;
}

function handleFileSelection(evt) {
    var file = $('#file').prop('files')[0];
    if (file) {
        $('#file-name').html(file.name);
    } else {
        $('#file-name').html('');
    }
    $('#file-name').show();
}

function handleForgotPasswordClick(evt) {
    var elem = $('<div>Are you sure?<br></div>');
    var confirm = $('<input type="button" value="Confirm" class="button is-primary" />');
    var cancel = $('<input type="button" value="Cancel" class="button" />');
    // when user clicks on 'Confirm', check if they are logged in. If they are, show an error message.
    // Otherwise, redirect them to the 'Forgot password' page.
    $(confirm).click(function (evt) {
        var options = {
            error: function (jqxhr, statusText, error) {
                hideLoginModal();
                showErrorMessage(jqxhr.status, jqxhr.responseText);
                $(elem).remove();
            },
            method: 'GET',
            success: function (data, statusText, jqxhr) {
                $(elem).remove();
                hideLoginModal();
                if (data.info.loggedIn) {
                    showNotification('You cannot use the "Forgot password" feature while logged in.', 'danger');
                } else {
                    window.location.replace('/user/forgot-password');
                }
            },
            url: '/user/am-i-logged-in'
        };
        jQuery.ajax(options);
    });
    // When user clicks 'Cancel', simply remove the message & buttons.
    $(cancel).click(function (evt) {
        $(elem).remove();
    });
    $(elem).append(confirm);
    $(elem).append(cancel);
    $('#forgot-password').parent().append(elem);
}

// called when the input method changes (text|file|zip etc...)
function handleInputMethodChange(evt) {
    // get the new input method
    var newMethod = evt.target.value;

    var text = $('#editor-container');
    var file = $('#select-file');
    var zip = $('#select-zip');
    var repo = $('#select-repo') || null;
    var fname = $('#file-name');
    var zname = $('#zip-name');
    var lang = $('#select-language');

    switch (newMethod) {
        case 'text':
            $(text).show();
            $(file).hide();
            $(zip).hide();
            $(fname).hide();
            $(zname).hide();
            $(lang).show();
            if(repo) $(repo.hide());
            break;
        case 'file':
            $(text).hide();
            $(file).show();
            $(zip).hide();
            $(fname).show();
            $(zname).hide();
            $(lang).show();
            if(repo) $(repo.hide());
            break;
        case 'zip':
            $(text).hide();
            $(file).hide();
            $(zip).show();
            $(fname).hide();
            $(zname).show();
            $(lang).hide();
            if(repo) $(repo.hide());
            break;
        case 'repo':
            $(text).hide();
            $(file).hide();
            $(zip).hide();
            $(fname).hide();
            $(zname).hide();
            $(lang).hide();
            $(repo).show();
    }
    // adjust footer after showing/hiding elements
    adjustFooter();
}

// called when the language changes (html|css etc...)
function handleLanguageChange(evt) {
    var newLang = evt.target.value;
    editor.getSession().setMode('ace/mode/' + newLang);
}

// click event handler for login button
function handleLoginClick(evt) {
    $('#login-modal').addClass('is-active');
}

function handleLoginError(status, response) {
    if (status % 400 < 100) {
        // HTTP client error 4xx
        var msg = 'Invalid login credentials.';
        try {
            msg = JSON.parse(response).errors.msg;
        } catch (ex) {}
        showLoginErrorMessage('Login error', msg);
    } else if (status % 500 < 100) {
        // HTTP server error 5xx
        var msg = 'Server error.';
        try {
            msg = JSON.parse(response).errors.msg;
        } catch (ex) {}
        hideLoginModal();
        showNotification(msg, 'danger', timeoutLength);
    }
}

function handleLoginSubmission(evt) {
    if(!allowLoginSubmission) {
        return false;
    }
    var url = evt.target.action;
    var email = $('#login-email').val();
    var password = $('#login-password').val();
    // check that both email and password were submitted
    if (email && password) {
        allowLoginSubmission = false;
        var obj = {
            email: email,
            password: password
        };
        var options = {
            data: obj,
            error: function (jqxhr, textStatus, error) {
                handleLoginError(jqxhr.status, jqxhr.responseText);
                allowLoginSubmission = true;
            },
            method: 'POST',
            success: function (data, statusText, jqxhr) {
                handleLoginSuccess(data);
                allowLoginSubmission = true;
            },
            url: url
        };
        jQuery.ajax(options);
    } else {
        // show an error message if some info missing
        var msgTitle = 'Incomplete Form';
        var errMsg = 'You must fill out all fields. You did not include ';
        errMsg += email ? 'a password.' : 'an email.';
        showLoginErrorMessage(msgTitle, errMsg);
    }
    // prevent form submission
    return false;
}

function handleLoginSuccess(res) {
    var msg = $('#login-message');
    // hide the login error message 
    $(msg).hide();
    // hide the login modal and reset the form
    $('#login-modal').removeClass('is-active');
    $('#login-email').val('');
    $('#login-password').val('');

    // show notification
    showNotification(res.message, 'success', timeoutLength);

    $('#login-btn').hide();
    $('#register-btn').hide();
    $('#logout-btn').show();
    $('#profile-btn').show();
}

function handleLogoutClick(evt) {
    if(!allowLogoutSubmission) {
        return false;
    }
    allowLogoutSubmission = false;
    var options = {
        error: function (jqxhr, textStatus, error) {
            if (jqxhr.status % 400 < 100) {
                // HTTP client error 4xx
                var msg = JSON.parse(jqxhr.responseText).errors.msg;
                showNotification(msg, 'warning', timeoutLength);
                allowLogoutSubmission = true;
            }
        },
        method: 'POST',
        success: function (data, statusText, jqxhr) {
            var res = JSON.parse(jqxhr.responseText);
            showNotification(res.message, 'info', timeoutLength);

            $('#login-btn').show();
            $('#register-btn').show();
            $('#logout-btn').hide();
            $('#profile-btn').hide();

            $('#select-repo').remove();
            var currInputMethod = $('#input-method').val();
            if(currInputMethod === 'repo') {
                $('#input-method').val('text').trigger('change');
            }
            $('#input-method option[value="repo"]').remove();

            allowLogoutSubmission = true;
        },
        url: '/user/logout'
    };
    jQuery.ajax(options);
}

function handleRegisterClick(evt) {
    $('#register-modal').addClass('is-active');
}

function handleRegisterSubmission(evt) {
    if(!allowRegisterSubmission) {
        return false;
    }
    var url = evt.target.action;
    // get the form data
    var email = $('#register-email').val();
    var password1 = $('#register-password1').val();
    var password2 = $('#register-password2').val();
    // check if the passwords match
    var match = password1 === password2;
    // element for displaying registration form errors
    var msg = $('#register-message');
    // check that all data filled out and passwords match
    if (email && password1 && password2 && match) {
        allowRegisterSubmission = false;
        var obj = {
            email: email,
            password1: password1,
            password2: password2
        };
        var options = {
            data: obj,
            error: function (jqxhr, textStatus, error) {
                var message = jqxhr.responseJSON.errors.msg;
                showRegisterErrorMessage('Error', message);
                allowRegisterSubmission = true;
            },
            method: 'POST',
            success: function (data, statusText, jqxhr) {
                var message = jqxhr.responseJSON.message;

                // hide the message
                $(msg).hide();
                // hide the modal and reset form values
                $('#register-modal').removeClass('is-active');
                $('#register-email').val('');
                $('#register-password1').val('');
                $('#register-password2').val('');

                showNotification(message, 'success', timeoutLength);
                allowRegisterSubmission = true;
            },
            url: url
        };
        jQuery.ajax(options);
        return false;
    } else {
        var errMsg, errTitle;
        // if the passwords match, that means not all data was filled out
        if (match) {
            // show appropriate message if form not filled out
            errTitle = 'Incomplete Form';
            errMsg = 'You must fill out all fields. You did not ';
            errMsg += email ? 'include a password.' : 'include an email.';
        } else {
            // show appropriate message if passwords don't match
            errTitle = 'Wrong password';
            errMsg = 'Your passwords do not match';
        }
        $('#register-message-header').html(errTitle);
        $('#regiser-message-body').html(errMsg);
        // show the message 
        $(msg).show();
        return false;
    }
}

// handles errors returned from the server
function handleValidationError(jqxhr, textStatus, error) {
    var errors = JSON.parse(jqxhr.responseText).errors;
    var out = 'Errors:<br>';
    for (var i = 0; i < errors.length; i++) {
        out += (i + 1) + '. ' + errors[i].msg;
    }
    showNotification(out, 'danger');
    allowValidationSubmission = true;
}

// handles the response received from the server
function handleValidationResponse(data, statusText, jqxhr) {
    var currMessage;
    var inputType = $('#input-method').val();
    // clear any error highlighting from previous validation
    clearAllHighlights();
    // remove any previous messages
    $('#output').empty();
    // clear file and zip name
    $('#file-name').hide();
    $('#zip-name').hide();
    var messages = data.valRes.messages;
    for(var i=0; i<messages.length; i++) {
        currMessage = messages[i];
        // highlight error and warnings if text validation
        if((currMessage.type === 'error' || currMessage.type === 'warning') && inputType === 'text') {
            highlightErrors(currMessage);
        }
    }
    // display the results
    showValidationMessages(messages);
    // show the medal the user received for the validation
    showMedal(data.report.medal);
    // clear any errors from previous requests
    hideNotification();
    allowValidationSubmission = true;
}

// event handling function for form submission
function handleValidationSubmission(evt) {
    if(!allowValidationSubmission) {
        return false;
    }
    // prevent form submission - use AJAX instead
    evt.preventDefault();
    // get the URL to send the request to 
    var url = evt.target.action;
    var inputType = $('#input-method').val();
    // get the content to be sent in the request
    var content = getInputContent();
    // if no content, show error and return
    if (!content) {
        showNotification('You must provide content to validate.', 'danger');
        return;
    }
    var language = $('#language').val();
    // append validation type to url
    url += '/' + inputType;
    // check that the file isn't too large
    if ((inputType === 'file' || inputType === 'zip') && !checkFileSize()) {
        showNotification('The file you are trying to upload is too large.', 'danger');
        return;
    }
    allowValidationSubmission = false;
    // create and send the request
    var headers = {};
    if (inputType === 'text') {
        content.lang = language;
        content = JSON.stringify(content);
    } else if(inputType === 'repo') {
        content = JSON.stringify(content);
    }
    var options = {
        contentType: 'application/json',
        data: content,
        error: handleValidationError,
        method: 'POST',
        processData: false,
        success: handleValidationResponse,
        url: url

    };
    if (inputType === 'file' || inputType === 'zip') {
        options.contentType = false;
    }
    jQuery.ajax(options);
}

function handleZipSelection(evt) {
    var zip = $('#zip').prop('files')[0];
    if (zip) {
        $('#zip-name').html(zip.name);
    } else {
        $('#zip-name').html('');
    }
    $('#zip-name').show();
}

function hideLoginModal(evt) {
    $('#login-modal').removeClass('is-active');
}

function hideNotification() {
    $('#notification-text').html('');
    $('#notification')[0].className = 'notification hidden';
    adjustFooter();
}

function hideRegisterModal(evt) {
    $('#register-modal').removeClass('is-active');
}

// highlights errors in the code editor
function highlightErrors(messageObj) {
    var row = messageObj.lastLine || 1;
    var firstCol = messageObj.firstColumn;
    var lastCol = messageObj.lastColumn;
    var currAnchor;
    var range = new Range(row - 1, firstCol, row - 1, lastCol);
    var markerStyle = messageObj.type === 'warning' ? 'code-warning-marker' : 'code-error-marker';
    var markerId = editor.getSession().addMarker(range, markerStyle, 'fullLine', false);
    var document = editor.getSession().getDocument();
    var anchor = new Anchor(document, row, 0);
    highlights.push({
        anchor: anchor,
        range: range,
        extract: messageObj.extract,
        marker: markerId
    });
}

function initCheckLogin() {
    var options = {
        method: 'GET',
        success: function (data, statusText, jqxhr) {
            if (data.info.loggedIn) {
                $('#logout-btn').show();
                $('#profile-btn').show();
            } else {
                $('#login-btn').show();
                $('#register-btn').show();
            }
        },
        url: '/user/am-i-logged-in'
    };
    jQuery.ajax(options);
}

function makeValidationMessage(message) {
    if(message) {
        // determine message class
        var msgClass = '';
        if(message.type === 'error') {
            msgClass = 'is-danger';
        } else if(message.type === 'warning') {
            msgClass = 'is-warning';
        }
        // create the message element
        var div = $('<div class="message ' + msgClass + '"></div>');
        var headerText = message.type;
        if(message.firstLine != null) {
            headerText += ' on line ' + message.firstLine;
        }
        var header = $('<div class="message-header">' + headerText + '</div>');
        var body = $('<div class="message-body"></div>');
        // if a 'file' argument was passed, display it
        if(message.file != null) {
            $(body).append($('<p><strong>File</strong></p><p>' + message.file + '</p>'));
        }
        // add the acutal error message
        $(body).append($('<p><strong>Message</strong></p><p>' + message.message + '</p>'));
        // if there is an extract, add it
        if(message.extract) {
            $(body).append($('<p><strong>Extract</strong></p><p>' + message.extract + '</p>'));
        }
        // add the error line (line x to line y if multiline error)
        var locationMessage = 'Line ' + message.firstLine;
        if(message.firstLine != message.lastLine) {
            locationMessage += ' to line ' + message.lastLine;
        }
        $(body).append($('<p><strong>Location</strong></p><p>' + locationMessage + '</p>'));
        // generate the message element and return it
        $(div).append(header);
        $(div).append(body);
        return div;
    } else {
        // if no message passed, create and return a success message
        var div = $('<div class="message is-success"></div>');
        var header = $('<div class="message-header">No errors found</div>');
        var body = $('<div class="message-body"><strong>Congratulations!</strong> No errors were found when analyzing your code.</div>');
        $(div).append(header);
        $(div).append(body);
        return div;
    }
}

function showErrorMessage(status, response) {
    var msg = status % 400 < 100 ? 'An error ocurred.' : 'An unexpected server error ocurred.';
    try {
        msg = JSON.parse(response).errors.msg;
    } catch (ex) {}
    showNotification(msg, 'danger', timeoutLength);
}

function showLoginErrorMessage(title, body) {
    var msg = $('#login-message');
    $('#login-message-header').html(title);
    $('#login-message-body').html(body);
    // show the message if it's not visible
    if (!$(msg).is(':visible')) {
        $(msg).show();
    }
}

function showMedal(medal) {
    $('#validation-medal').empty();
    var imgPath = '/images/';
    if(medal === 'GOLD') {
        imgPath += 'gold-medal.png';
    } else if(medal === 'SILVER') {
        imgPath += 'silver-medal.png';
    } else {
        imgPath += 'bronze-medal.png';
    }
    $('<img src="' + imgPath + '" alt="validation medal">').load(function() {
        $(this).appendTo('#validation-medal');
    });
}

function showNotification(text, style, timeout) {
    $('#notification-text').html(text);
    $('#notification')[0].className = 'notification ' + 'is-' + style;
    // if timeout provided, hide the notification after specified timeout
    if (timeout) {
        setTimeout(hideNotification, timeout);
    }
}

function showRegisterErrorMessage(title, body) {
    var msg = $('#register-message');
    $('#register-message-header').html(title);
    $('#register-message-body').html(body);
    // show the message if it's not visible
    if (!$(msg).is(':visible')) {
        $(msg).show();
    }
}

function showValidationMessages(messages) {
    if(messages.length > 0) {
        for(var i=0; i<messages.length; i++) {
            $('#output').append(makeValidationMessage(messages[i]));
        }
    } else {
        $('#output').append(makeValidationMessage());
    }
    resetFooter();
    adjustFooter();
}

function triggerFileClick(evt) {
    $('#file').click();
}

function triggerZipClick(evt) {
    $('#zip').click();
}

// --------------------------------------------------------------------------
// ------------------------- initialization function ------------------------
// --------------------------------------------------------------------------
(function () {
    // create editor for text input
    editor = ace.edit('editor');
    editor.setTheme('ace/theme/github');
    editor.getSession().setMode('ace/mode/html');
    editor.getSession().setUseWrapMode(true);
    editor.setOption('dragEnabled', false);
    editor.getSession().on('change', clearAllHighlights);

    $('#fake-file').click(triggerFileClick);
    $('#fake-zip').click(triggerZipClick);
    $('#file').change(handleFileSelection);
    $('#zip').change(handleZipSelection);

    // input method change event listener
    $('#input-method').change(handleInputMethodChange);
    // language change event listener
    $('#language').change(handleLanguageChange);
    // form submission event listener
    $('#form').submit(handleValidationSubmission);

    // check if the user is currently logged in 
    initCheckLogin();

    // login button click listener
    $('#login-btn').click(handleLoginClick);
    // register button click listener
    $('#register-btn').click(handleRegisterClick);
    // logout button click listener
    $('#logout-btn').click(handleLogoutClick);

    $('#hide-login-modal').click(hideLoginModal);
    $('#hide-register-modal').click(hideRegisterModal);

    $('#login-form').submit(handleLoginSubmission);
    $('#register-form').submit(handleRegisterSubmission);

    $('#forgot-password').click(handleForgotPasswordClick);

    $('#hide-notification').click(hideNotification);

    resetFooter();
})();
