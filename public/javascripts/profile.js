function handleLogoutClick() {
    var options = {
        error: function (jqxhr, textStatus, error) {
            if (jqxhr.status % 400 < 100) {
                // HTTP client error 4xx
                var msg = jqxhr.responseJSON.errors.msg;
                showNotification(msg, 'warning', timeoutLength);
            }
        },
        method: 'POST',
        success: function (data, statusText, jqxhr) {
            // if logged out, redirect back to home page
            window.location.replace('/');
        },
        url: '/user/logout'
    };
    jQuery.ajax(options);
}

function handleReportMinimize(evt) {
    var span = $(evt.target);
    var message = $($($(span).parent()).parent().parent());
    var body = $(message).find('.message-body');
    var icon = $(message).find('.report-minimize').find('.fa');
    if($(body).is(':visible')) {
        $(body).hide();
        $(icon).removeClass('fa-minus');
        $(icon).addClass('fa-plus');
    } else {
        $(body).show();
        $(icon).removeClass('fa-plus');
        $(icon).addClass('fa-minus');
    }
    adjustFooter();
}

function handleReportsTabClick() {
    var reports = $('#reports');
    if(!$(reports).is(':visible')) {
        $($('#reports-tab').parent()).toggleClass('is-active');
        $($('#repos-tab').parent()).toggleClass('is-active');
        $('#repos').hide();
        $('#reports').show();
        adjustFooter();
    }
}

function handleReposTabClick() {
    var repos = $('#repos');
    if(!$(repos).is(':visible')) {
        $($('#repos-tab').parent()).toggleClass('is-active');
        $($('#reports-tab').parent()).toggleClass('is-active');
        $('#reports').hide();
        $('#repos').show();
        adjustFooter();
    }
}

function handleRepoNameClick(event) {
    var data = {
        repoName: $(event.target).text()
    };
    var options = {
        data: data,
        error: function(jqxhr, textStatus, error) {
            showNotification('An error ocurred.', 'danger', timeoutLength);
        },
        method: 'POST',
        success: function(data, statusText, jqxhr) {
            let reportId = data.report.id;
            window.location.replace('/v/view-report/' + reportId);
        },
        url: '/v/repo'
    };
    jQuery.ajax(options);
}

function hideNotification() {
    $('#notification-text').html('');
    $('#notification')[0].className = 'notification hidden';
}

function showButtons() {
    $('#logout-btn').show();
    $('#profile-btn').show();
}

function showNotification(text, style, timeout) {
    $('#notification-text').html(text);
    $('#notification')[0].className = 'notification ' + 'is-' + style;
    // if timeout provided, hide the notification after specified timeout
    if (timeout) {
        setTimeout(hideNotification, timeout);
    }
}

(function() {
    showButtons();

    $('#repos-tab').click(handleReposTabClick);
    $('#reports-tab').click(handleReportsTabClick);

    $('.report-minimize').click(handleReportMinimize);

    $('#logout-btn').click(handleLogoutClick);

    $('.repo-name').click(handleRepoNameClick);
})();
