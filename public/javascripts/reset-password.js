var allowFormSubmission = true;

function handleFormSubmission(evt) {
    if(!allowFormSubmission) {
        return false;
    }
    var password1 = $('#password1').val();
    var password2 = $('#password2').val();

    // make sure passwords were filled in
    if (password1 && password2) {
        // make sure that the passwords match
        if (password1 === password2) {
            allowFormSubmission = false;
            var obj = {
                password1: password1, password2: password2
            };
            var randStr = window.location.href.split('/').pop();
            var options = {
                data: obj,
                error: function(jqxhr, textStatus, error) {
                    var res = JSON.parse(jqxhr.responseText);
                    var msg = res.errors.msg;
                    showMessage('Error', msg, 'danger');
                    allowFormSubmission = true;
                },
                method: 'POST',
                success: function(data, statusText, jqxhr) {
                    var msg = data.message;
                    showMessage('Success', msg, 'success');
                    $('#password1').val('');
                    $('#password1').val('');
                    allowFormSubmission = true;
                },
                url: '/user/reset-password/' + randStr 
            };
            jQuery.ajax(options);
        } else {
            showMessage('Invalid Form', 'The passwords entered do not match.', 'danger');
        }
    } else {
        showMessage('Invalid Form', 'You must fill out all form fields.', 'danger');
    }

    return false;
}

(function() {
    $('#form').submit(handleFormSubmission);
})();
