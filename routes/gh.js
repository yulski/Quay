const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const multer = require('multer');
const fecha = require('fecha');
const jwt = require('jsonwebtoken');
const unirest = require('unirest');

const fh = require('../src/file-handling');
const User = require('../models').User;

const PRIVATE_KEY = process.env.GH_INTEGRATION_PRIVATE_KEY || require('../config').GH_INTEGRATION_PRIVATE_KEY; // TODO change this - it only works in dev environment
const INTEGRATION_ID = process.env.GH_INTEGRATION_ID;

// this is the route that GitHub sends payload to for integration validation
router.post('/webhook', (req, res, next) => {

    // check that a repository and installation fields are there
    req.checkBody('repository', 'No repository field.').notEmpty();
    req.checkBody('installation', 'No installation field').notEmpty();

    req.getValidationResult().then(reqValRes => {
        // check that req body is ok
        if (reqValRes.isEmpty()) {
            let repoName = req.body.repository.name;
            // save repo name in request to be passed to next middleware func
            req.repoName = repoName;
            let repoId = req.body.repository.id;
            // save repo id in request to be passed to next middleware func
            req.repoId = repoId;
            let userGhId = req.body.repository.owner.id;
            let installationId = req.body.installation.id;
            // save installation id & commit sha in request to be passed to next middleware func
            req.installationId = installationId;
            req.sha = req.body.head_commit.id;

            // find the user whose repo is being validated
            User.findOne({
                where: {
                    ghid: userGhId
                }
            }).then(user => {
                if (user) {
                    // log the user in to authenticate this request
                    req.login(user, err => {
                        if (!err) {
                            let redis = req.app.get('redis');
                            // check if a previous token exists in redis
                            redis.get(installationId, (err, reply) => {
                                if (reply) {
                                    // if installation token found, go to next middleware function
                                    next();
                                } else if (!err) {
                                    // if installation token not found in redis, must obtain one
                                    // from GitHub

                                    // dates for JWT
                                    let now = parseInt(new Date().getTime() / 1000);
                                    let expiry = now + (59 * 10);
                                    // JWT payload
                                    let payload = {
                                        iat: now,
                                        exp: expiry,
                                        iss: INTEGRATION_ID
                                    };
                                    jwt.sign(payload, PRIVATE_KEY, {
                                        algorithm: 'RS256'
                                    }, (err, jwtToken) => {
                                        if (!err) {
                                            // get installation token from GitHub
                                            req.app.get('gh').getInstallationToken(installationId, jwtToken)
                                                .then(authRes => {
                                                    // save it in redis, set expiry, and contine to next middleware
                                                    redis.set(installationId, authRes.token, () => {
                                                        redis.expireat(installationId, expiry, () => {
                                                            next();
                                                        });
                                                    });
                                                });
                                        } else {
                                            // a whole bunch of error handlers now...
                                            res.status(500).send('Server error!');
                                        }
                                    });
                                } else {
                                    res.status(500).send('Server error');
                                }
                            });
                        } else {
                            res.status(500).send('Server error');
                        }
                    });
                } else {
                    res.status(400).send('You must register first.');
                }
            }).catch(err => {
                console.error(err);
                res.status(500).send('Server error');
            });
        } else {
            // if req invalid, send req validation errors
            res.send(reqValRes.array());
        }
    });
}, (req, res, next) => {
    let redis = req.app.get('redis');
    // get the installation token from redis
    redis.get(req.installationId, (err, reply) => {
        if (reply && !err) {
            // validate repo
            req.app.get('validation').validateRepo(req.user, req.repoName, reply).then(resObj => {
                // send validation response
                res.send(resObj.valRes);
                // create status based on validation result
                let status = 'pending';
                if (resObj.valRes.messages.length) {
                    status = 'error';
                } else {
                    status = 'success';
                }
                let appUrl = process.env.APP_URL;
                let url = `${appUrl}/user/auth-view-report/${resObj.reportId}`;
                req.app.get('gh').createStatus(req.repoId, req.sha, reply, status, url);
            }).catch(err => {
                res.status(500).send({
                    errors: [{
                        msg: 'An unexpected server error ocurred.'
                    }]
                });
            });
        } else {
            res.status(500).send('Server error');
        }
    });
});

module.exports = router;
