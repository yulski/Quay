const express = require('express');
const router = express.Router();

// GET home page
router.get('/', (req, res, next) => {
  let repos = req.session.repos;
  // allow repo validation if GitHub user logged in
  let allowRepoValidation = req.user && req.user.ghid && repos;
  res.render('index', {
    buttons: true,
    repos: repos,
    allowRepoValidation: allowRepoValidation
  });
});

module.exports = router;
