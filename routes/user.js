// ------------------------------------------------------------------------------------------------
// ------------------------------ imports from external dependencies ------------------------------
// ------------------------------------------------------------------------------------------------
const express = require('express');
const router = express.Router();
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const postmark = require('postmark')(process.env.POSTMARK_API_TOKEN);
const randomstring = require('randomstring');
const bcrypt = require('bcrypt');
const fecha = require('fecha');
const GithubStrategy = require('passport-github2').Strategy;
const unirest = require('unirest');

// ------------------------------------------------------------------------------------------------
// ----------------------------------- imports from this project ----------------------------------
// ------------------------------------------------------------------------------------------------
const User = require('../models').User;
const AccessKey = require('../models').AccessKey;
const saltRounds = +process.env.SALT_ROUNDS || 12;

const userFilter = require('../filters/user');

// ------------------------------------------------------------------------------------------------
// ----------------------------------------- constants --------------------------------------------
// ------------------------------------------------------------------------------------------------
const invalidKeyMessage = 'You do not have a valid key.';
const serverErrorMessage = 'An unexpected server error ocurred. Please try again later. If the issue persists, you should contact the site\'s administrator.';
const emailSendingErrorMessage = 'An error ocurred when attempting to send an email to the address provided.';

const appUrl = process.env.APP_URL;
const appEmail = process.env.APP_EMAIL;

// ------------------------------------------------------------------------------------------------
// ------------------------------------- access key functions -------------------------------------
// ------------------------------------------------------------------------------------------------
function isKeyActive(key) {
    let expires = new Date(key.expires);
    let now = new Date();
    return expires.getTime() > now.getTime();
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------ GET routes ------------------------------------------
// ------------------------------------------------------------------------------------------------

// checks if user logged in and sends response
router.get('/am-i-logged-in', (req, res) => {
    var loggedIn = false;
    var email = null;
    if (req.user) {
        loggedIn = true;
        email = req.user.email;
    }
    res.send({
        message: 'OK',
        info: {
            loggedIn: loggedIn,
            email: email
        }
    });
});

// save redirect in session and redirect to GH auth route
router.get('/auth-view-report/:id', userFilter.ensureNotLoggedIn(), (req, res, next) => {
    req.session.redirectTo = `${process.env.APP_URL}/v/view-report/${req.params.id}`;
    res.redirect('/user/gh-auth');
});

// 1st stage of GitHub authentication - send auth request to GitHub
router.get('/gh-auth', passport.authenticate('github', {
    scope: ['user']
}));

// 2nd stage of GitHub authentication - GitHub sent back response and have to 
// handle it now
router.get('/gh-callback', (req, res, next) => {
    // use passport to authenticate
    passport.authenticate('github', (err, user, info) => {
        if (err) {
            req.flash('error', 'An error occurred while trying to log you in.');
            return res.redirect('/');
        }
        if (!user) {
            req.flash('error', 'Failed to authenticate with GitHub.');
            return res.redirect('/');
        }
        // manually log user in
        req.logIn(user, (err) => {
            if (err) {
                return next(err);
            }
            req.flash('success', 'Logged in successfully.');
            let redirectPath = '/';
            // if there's a redirect in session, clear it and redirect there after
            // this is done
            if (req.session.redirectTo) {
                redirectPath = req.session.redirectTo;
                delete req.session.redirectTo;
            }
            // get user GH repos & validation reports & medal and save them in session
            // I think this is concurrent
            new Promise((fulfill, reject) => {
                let notDone = 2;
                if (user.ghtoken) {
                    req.app.get('gh').getUserRepos(user.ghuname, user.ghtoken).then(repos => {
                        req.session.repos = repos;
                        notDone--;
                        if (notDone === 0) fulfill();
                    });
                } else {
                    req.session.repos = null;
                    notDone--;
                    if (notDone === 0) fulfill();
                }
                req.app.get('reports').getUserReports(user).then(userReports => {
                    req.session.userReports = userReports;
                    req.session.userMedal = req.app.get('medals').generateUserMedal(userReports);
                    notDone--;
                    if (notDone === 0) fulfill();
                });
            }).then(() => {
                return res.redirect(redirectPath);
            });
        });
    })(req, res, next);
});

// show forgot password form
router.get('/forgot-password', userFilter.ensureNotLoggedIn(), (req, res) => {
    res.render('forgot-password', {
        buttons: false
    });
});

// user profile page
router.get('/profile', userFilter.ensureLoggedIn(), (req, res, next) => {
    // get GH repos, medal, and validation reports from session
    // to show them on page
    let repos = req.session.repos;
    let medal = req.session.userMedal;
    let reports = req.session.userReports;
    let args = {
        buttons: true,
        isGuthubUser: req.user.ghid ? true : false,
    };
    if (repos) {
        args.repos = repos;
    }
    if (medal) {
        args.medal = medal;
    }
    if (reports) {
        args.reports = reports;
    }
    res.render('profile', args);
});

// refresh GitHub repos - the list of repos is taken from GitHub at login, so if the user
// creates any repos after logging in they will not be shown. they have to click refresh
// to get the repo list again
router.get('/profile/refresh-repos', userFilter.ensureLoggedIn(), (req, res, next) => {
    let token = req.user.ghtoken;
    // get repos + save them in session
    req.app.get('gh').getUserRepos(req.user.ghuname, token).then((repos) => {
        req.session.repos = repos;
        res.redirect('/user/profile');
    });
});

// show password reset page (the form to reset password). the user should get here
// after clicking link in email
router.get('/reset-password/:randStr', (req, res, next) => {
    // check that the randStr format is ok
    let randStr = req.params.randStr;
    req.checkParams('randStr', 'A key is required.').notEmpty();
    req.checkParams('randStr', 'The key is not in the correct format.').isAlphanumeric();

    req.getValidationResult().then((result) => {
        if (result.isEmpty()) {
            let breakLoop = false;
            // find all RESETs in DB
            AccessKey.findAll({
                where: {
                    type: 'password_reset'
                }
            }).then((resets) => {
                let loops = 0;
                // loop through the resets
                for (let reset of resets) {
                    if (breakLoop) {
                        break;
                    }
                    // make sure expiry date on the reset has not passed
                    if (isKeyActive(reset)) {
                        // compare randStr with each reset's hash
                        bcrypt.compare(randStr, reset.hash, (err, equal) => {
                            loops++;
                            if (!err) {
                                if (equal && !breakLoop) {
                                    // if valid DB entry found, render the reset-password page
                                    res.render('reset-password', {
                                        buttons: false
                                    });
                                    breakLoop = true;
                                } else if (loops === resets.length && !breakLoop) {
                                    // if all resets have been looped through and no match found, show error message
                                    res.status(400).send({
                                        errors: {
                                            msg: invalidKeyMessage
                                        }
                                    })
                                    breakLoop = true;
                                }
                            } else if (!breakLoop) {
                                // handle hashing error
                                res.status(500).send({
                                    errors: {
                                        msg: serverErrorMessage
                                    }
                                })
                                breakLoop = true;
                            }
                        });
                    } else if (!breakLoop) {
                        // if reset expired, increase loop count
                        loops++;
                        // if last loop and reset expired, return error
                        if (loops === resets.length) {
                            breakLoop = true;
                            // if all resets have been looped through and no match found, show error message
                            res.status(400).send({
                                errors: {
                                    msg: invalidKeyMessage
                                }
                            })
                        }
                    }
                }
            });
        } else {
            // send errors when randStr format is incorrect
            res.status(400).send({
                errors: result.useFirstErrorOnly().array()
            });
        }
    });
});

// password verification - the user should get here after clicking link in email
router.get('/verify-email/:randStr', (req, res) => {
    let randStr = req.params.randStr;

    // validate randStr
    req.checkParams('randStr', 'You do not have a valid key.').notEmpty().isAlphanumeric();

    req.getValidationResult().then((result) => {
        if (result.isEmpty()) {
            // get all email verification keys
            AccessKey.findAll({
                where: {
                    type: 'email_verification'
                }
            }).then((keys) => {
                let loops = 0;
                let breakLoop = false;

                if (keys.length === 0) {
                    res.status(400).send({
                        errors: {
                            msg: invalidKeyMessage
                        }
                    });
                } else {

                    // loop through the keys
                    for (let key of keys) {
                        if (breakLoop) {
                            break;
                        }
                        // check that the key is not expired
                        if (isKeyActive(key)) {
                            // find key whose hash matches randStr
                            bcrypt.compare(randStr, key.hash, (err, same) => {
                                loops++;
                                if (!err) {
                                    // if matching key found
                                    if (same && !breakLoop) {
                                        breakLoop = true;
                                        let userId = key.user;
                                        // find user associated with the key
                                        User.findOne({
                                            where: {
                                                id: userId
                                            }
                                        }).then((user) => {
                                            if (user) {
                                                // change the user to a full user
                                                user.update({
                                                    full: true
                                                }).then(() => {
                                                    // send success response and remove the access_key from the database
                                                    red.render('email-verification', {
                                                        buttons: false,
                                                        messageStyle: 'success',
                                                        messageHeader: 'Success',
                                                        messageBody: 'Your email has been successfully verified.'
                                                    });
                                                    key.destroy({
                                                        force: true
                                                    });
                                                }).catch((err) => {
                                                    // send error response on database error
                                                    res.status(500).send({
                                                        errors: {
                                                            msg: serverErrorMessage
                                                        }
                                                    })
                                                });
                                            } else {
                                                // send error response if no user found
                                                res.status(400).send({
                                                    errors: {
                                                        msg: invalidKeyMessage
                                                    }
                                                })
                                            }
                                        }).catch((err) => {
                                            // send error response in case of database error
                                            res.status(500).send({
                                                errors: {
                                                    msg: serverErrorMessage
                                                }
                                            });
                                        });
                                    }
                                    // if key not matching and this is last loop
                                    else if (loops === keys.length && !breakLoop) {
                                        breakLoop = true;
                                        res.status(500).send({
                                            errors: {
                                                msg: serverErrorMessage
                                            }
                                        });
                                    }
                                } else if (!breakLoop) {
                                    // send error response in case of bcrypt error
                                    res.status(500).send({
                                        errors: {
                                            msg: serverErrorMessage
                                        }
                                    });
                                }
                            });
                        } else if (!breakLoop) {
                            // if key expired, increase loop count
                            loops++;
                            // if last loop and key expired, send error
                            if (loops === keys.length) {
                                breakLoop = true;
                                res.status(400).send({
                                    errors: {
                                        msg: invalidKeyMessage
                                    }
                                });
                            }
                        }
                    }
                }
            }).catch((err) => {
                // send error response in case of database error
                res.status(500).send({
                    errors: {
                        msg: serverErrorMessage
                    }
                });
            });
        } else {
            // send error response if randStr is invalid
            res.status(400).send({
                errors: result.array()[0]
            });
        }
    });

});

// ------------------------------------------------------------------------------------------------
// ------------------------------------------ POST routes -----------------------------------------
// ------------------------------------------------------------------------------------------------

// user get here by clicking 'forgot password'. an email w/ a reset link
// will be sent to their email
router.post('/forgot-password', userFilter.ensureNotLoggedIn(), (req, res) => {
    let email = req.body.email;

    // validate the email address
    req.checkBody('email', 'Email is required.').notEmpty();
    req.checkBody('email', 'Email must be a valid email address.').isEmail();

    req.getValidationResult().then((result) => {
        if (result.isEmpty()) {
            User.findOne({
                where: {
                    email: email
                }
            }).then((user) => {
                if (user) {
                    let now = new Date();
                    let tomorrow = new Date();
                    tomorrow.setDate(now.getDate() + 1);
                    // format dates into MySQL DATETIME format
                    let created = fecha.format(now, 'YYYY-MM-DD hh:mm:ss');
                    let expires = fecha.format(tomorrow, 'YYYY-MM-DD hh:mm:ss');
                    let id = user.id;

                    AccessKey.findAll({
                        where: {
                            user: id,
                            type: 'password_reset'
                        }
                    }).then((userKeys) => {
                        let activeUsersKeys = userKeys.filter((userKey) => isKeyActive(userKey));
                        if (activeUsersKeys.length === 0) {
                            req.app.get('randstr').generateUniqueRandStr('password_reset').then((randStr) => {
                                bcrypt.hash(randStr, saltRounds, (err, hash) => {
                                    if (!err) {
                                        AccessKey.create({
                                            hash: hash,
                                            user: id,
                                            created: created,
                                            expires: expires,
                                            type: 'password_reset'
                                        }).then((reset) => {
                                            if (reset) {
                                                let href = appUrl + '/user/reset-password/' + randStr;
                                                res.render('reset-password-email', {
                                                    href: href
                                                }, (err, html) => {
                                                    if (!err && html) {
                                                        postmark.send({
                                                            'From': appEmail,
                                                            'To': email,
                                                            'Subject': 'Forgot password',
                                                            'HtmlBody': html,
                                                            'Tag': 'Reset Password'
                                                        }, (error, success) => {
                                                            if (!error) {
                                                                res.send({
                                                                    message: 
                                                                        'An email containing a link that enables you ' + 
                                                                        'to change your password has been sent to ' + 
                                                                        email + '.',
                                                                    info: {}
                                                                });
                                                            } else {
                                                                // show error message
                                                                console.log('ERROR: failed to send email');
                                                                res.status(500).send({
                                                                    errors: {
                                                                        msg: emailSendingErrorMessage
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        res.status(500).send({
                                                            errors: {
                                                                msg: serverErrorMessage
                                                            }
                                                        });
                                                    }
                                                });
                                            } else {
                                                // if DB opreation failed, send error message
                                                res.status(500).send({
                                                    errors: {
                                                        msg: serverErrorMessage
                                                    }
                                                });
                                            }
                                        }).catch((err) => {
                                            res.status(500).send({
                                                errors: {
                                                    msg: serverErrorMessage
                                                }
                                            });
                                        });
                                    } else {
                                        res.status(500).send({
                                            errors: {
                                                msg: serverErrorMessage
                                            }
                                        });
                                    }
                                });
                            });
                        } else {
                            res.status(400).send({
                                errors: {
                                    msg: 'A password reset link has already been emailed to you in the last 24 hours.'
                                }
                            });
                        }
                    }).catch((err) => {
                        res.status(500).send({
                            errors: {
                                msg: serverErrorMessage
                            }
                        });
                    });
                }
            });
        } else {
            // return errors
            res.status(400).send({
                errors: result.useFirstErrorOnly().array()
            });
        }
    });
});

// login w/ email + password
router.post('/login', userFilter.ensureNotLoggedIn(), passport.authenticate('login'), (req, res, next) => {
    // get user GH repos + validation reports + medal and save them in session
    // I think this is concurrent...
    new Promise((fulfill, reject) => {
        let notDone = 2;
        if (req.user.ghtoken) {
            req.app.get('gh').getUserRepos(req.user.ghuname, req.user.ghtoken).then(repos => {
                req.session.repos = repos;
                notDone--;
                if (notDone === 0) fulfill();
            });
        } else {
            req.session.repos = null;
            notDone--;
            if (notDone === 0) fulfill();
        }
        req.app.get('reports').getUserReports(req.user).then(userReports => {
            req.session.userReports = userReports;
            req.session.userMedal = req.app.get('medals').generateUserMedal(userReports);
            notDone--;
            if (notDone === 0) fulfill();
        });
    }).then(() => {
        // success!
        res.send({
            message: 'Logged in successfully.'
        });
    });
});

// log user out
router.post('/logout', userFilter.ensureLoggedIn(), (req, res, next) => {
    req.logout();
    // delete user info saved in session
    delete req.session.repos;
    delete req.session.userMedal;
    delete req.session.userReports;
    res.send({
        message: 'Logged out successfully.',
        info: {}
    });
});

// register new user
router.post('/register', userFilter.ensureNotLoggedIn(), (req, res) => {
    // validate email and passwords
    req.checkBody('email', 'You must supply a valid email address.').notEmpty().isEmail();
    req.checkBody('password1', 'You must supply a valid password.').notEmpty().isValidPassword();
    req.checkBody('password2', 'You must supply a valid password.').notEmpty().isValidPassword();

    let email = req.body.email;
    let password1 = req.body.password1;
    let password2 = req.body.password2;

    // check that passwords match
    req.checkBody('password2', 'Your passwords do not match.').equals(password1);

    req.getValidationResult().then((result) => {
        // if there are no validation errors
        if (result.isEmpty()) {
            // see if a user is already registered with the submitted email address
            User.findOne({
                where: {
                    email: email
                }
            }).then((user) => {
                if (!user) {
                    // hash the submitted password
                    bcrypt.hash(password1, saltRounds, (err, hashedPassword) => {
                        if (!err) {
                            // create a new User in DB
                            User.create({
                                email: email,
                                password: hashedPassword,
                                full: false
                            }).then((newUser) => {
                                if (newUser) {
                                    req.app.get('randstr').generateUniqueRandStr('email_verification').then(randStr => {
                                        bcrypt.hash(randStr, saltRounds, (err, hash) => {
                                            if (!err) {
                                                let now = new Date();
                                                let tomorrow = new Date();
                                                tomorrow.setDate(now.getDate() + 1);
                                                // format dates into MYSQL DATETIME format
                                                let created = fecha.format(now, 'YYYY-MM-DD hh:mm:ss');
                                                let expires = fecha.format(tomorrow, 'YYYY-MM-DD hh:mm:ss');
                                                AccessKey.create({
                                                    hash: hash,
                                                    user: newUser.id,
                                                    created: created,
                                                    expires: expires,
                                                    type: 'email_verification'
                                                }).then((key) => {
                                                    if (key) {
                                                        let href = appUrl + '/user/verify-email/' + randStr;
                                                        res.render('verify-email-email', {
                                                            href: href
                                                        }, (err, html) => {
                                                            if (!err && html) {
                                                                // send the email
                                                                postmark.send({
                                                                    'From': appEmail,
                                                                    'To': email,
                                                                    'Subject': 'Verify your email',
                                                                    'HtmlBody': html,
                                                                    'Tag': 'Verify Email'
                                                                }, (error, success) => {
                                                                    if (!error) {
                                                                        res.send({
                                                                            message: 
                                                                            'An email containing a link to ' + 
                                                                            'verify your email address has been sent to' 
                                                                            + ' ' + email + '.',
                                                                            info: {}
                                                                        });
                                                                    } else {
                                                                        // a disturbing amount of error handlers now...

                                                                        // show error message
                                                                        console.log('ERROR: failed to send email');
                                                                        res.status(500).send({
                                                                            errors: {
                                                                                msg: emailSendingErrorMessage
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            } else {
                                                                res.status(500).send({
                                                                    errors: {
                                                                        msg: serverErrorMessage
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        res.status(500).send({
                                                            errors: {
                                                                msg: serverErrorMessage
                                                            }
                                                        });
                                                    }
                                                }).catch((err) => {
                                                    res.status(500).send({
                                                        errors: {
                                                            msg: serverErrorMessage
                                                        }
                                                    });
                                                });
                                            } else {
                                                res.status(500).send({
                                                    errors: {
                                                        msg: serverErrorMessage
                                                    }
                                                });
                                            }
                                        });
                                    }).catch((err) => {
                                        res.status(500).send({
                                            errors: {
                                                msg: serverErrorMessage
                                            }
                                        });
                                    });
                                } else {
                                    // send error response if failed to create user
                                    res.status(500).send({
                                        errors: {
                                            msg: serverErrorMessage
                                        }
                                    });
                                }
                            }).catch((err) => {
                                // send error response if failed to create user
                                res.status(500).send({
                                    errors: {
                                        msg: serverErrorMessage
                                    }
                                });
                            });
                        } else {
                            // send error response on bcrypt error
                            res.status(500).send({
                                errors: {
                                    msg: serverErrorMessage
                                }
                            });
                        }
                    });
                } else {
                    // send error response if the email is already in use
                    res.status(400).send({
                        errors: {
                            msg: 'The email address provided is already in use.'
                        }
                    });
                }
            }).catch((err) => {
                // send error response on database error
                res.status(500).send({
                    errors: {
                        msg: serverErrorMessage
                    }
                });
            });
        } else {
            res.status(400).send({
                errors: result.useFirstErrorOnly().array()[0]
            });
        }
    });
});

// password reset requests - if valid, the users password will be changed
router.post('/reset-password/:randStr', (req, res, next) => {
    let password1 = req.body.password1;
    let password2 = req.body.password2;
    let randStr = req.params.randStr;

    let passwordMessage = 'A valid password is required. A password must be between 8 and 99 characters long and must contain a lowercase letter, an uppercase letter and a number.';

    // validate the passwords
    req.checkBody('password1', passwordMessage).notEmpty().isValidPassword();
    req.checkBody('password2', passwordMessage).notEmpty().isValidPassword();
    req.checkBody('password1', 'Your passwords do not match.').equals(password2);

    // validate randStr
    req.checkParams('randStr', 'A key is required.').notEmpty();
    req.checkParams('randStr', 'The key is not in the correct format.').isAlphanumeric();

    req.getValidationResult().then((result) => {
        // check that there are no validation errors
        if (result.isEmpty()) {
            // get all reset access keys from DB
            AccessKey.findAll({
                where: {
                    type: 'password_reset'
                }
            }).then((resets) => {
                // if there are no resets, send back error
                if (resets.length === 0) {
                    res.status(400).send({
                        errors: {
                            msg: invalidKeyMessage
                        }
                    });
                }
                let loops = 0;
                let breakLoop = false;
                // loop through all resets
                for (let reset of resets) {
                    if (breakLoop) {
                        break;
                    }
                    // make sure that the key is not expired
                    if (isKeyActive(reset)) {
                        // compare randStr with hash of each password reset key 
                        bcrypt.compare(randStr, reset.hash, (err, same) => {
                            loops++;
                            if (!err) {
                                // if randStr matches a reset hash
                                if (same) {
                                    breakLoop = true;
                                    // get id of user associated with current reset
                                    let userId = reset.user;
                                    // find user by id
                                    User.findById(userId).then((user) => {
                                        // hash the submitted password and change the users password to that
                                        bcrypt.hash(password1, saltRounds, (err, pass) => {
                                            if (!err) {
                                                user.update({
                                                    password: pass
                                                }).then(() => {
                                                    // send 'OK' and remove the password reset key entry from the database
                                                    res.send({
                                                        message: 'Your password has been successfully changed.',
                                                        info: {}
                                                    });
                                                    reset.destroy({
                                                        force: true
                                                    });
                                                });
                                            } else {
                                                // send an error message when bcrypt error occurrs
                                                breakLoop = true;
                                                res.status(500).send({
                                                    errors: {
                                                        msg: serverErrorMessage
                                                    }
                                                });
                                            }
                                        });
                                    });
                                } else if (loops === resets.length) {
                                    // if no match and final loop, send error message
                                    breakLoop = true;
                                    res.status(400).send({
                                        errors: {
                                            msg: invalidKeyMessage
                                        }
                                    });
                                }
                            } else {
                                // send error message when bcrypt error occurrs
                                breakLoop = true;
                                res.status(500).send({
                                    errors: {
                                        msg: serverErrorMessage
                                    }
                                });
                            }
                        });
                    } else if (!breakLoop) {
                        // if key expired, increase loop count
                        loops++;
                        // if last loop and key expired, send error
                        if (loops === resets.length) {
                            breakLoop = true;
                            res.status(400).send({
                                errors: {
                                    msg: invalidKeyMessage
                                }
                            });
                        }
                    }
                }
            });
        } else {
            // send back error if request is invalid
            res.status(400).send({
                errors: result.array()[0]
            });
        }
    });
});

module.exports = router;
