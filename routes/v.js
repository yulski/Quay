const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');
const multer = require('multer');
const fecha = require('fecha');

const validation = require('../src/validation');
const fh = require('../src/file-handling');

const userFilter = require('../filters/user');
const uploadFilter = require('../filters/upload');

// view a single validation report
router.get('/view-report/:id', userFilter.ensureLoggedIn(), (req, res, next) => {
    let id = req.params.id;

    req.checkParams('id', 'Report id is required.').notEmpty().isInt();

    // validate request
    req.getValidationResult().then((result) => {
        if (result.isEmpty()) {
            // get the requested report
            req.app.get('reports').getSingleReport(id).then(report => {
                if (report) {
                    // check that the user is the report's owner
                    let user = req.user;
                    if (report.user == user.id) {
                        res.render('view-report', {
                            report: report
                        });
                    } else {
                        res.render('view-report', {
                            report: null
                        });
                    }
                } else {
                    res.render('view-report', {
                        report: null
                    });
                }
            });
        } else {
            res.status(400).send({
                errors: result.array()
            });
        }
    });
});

// route for text validation
router.post('/text', (req, res, next) => {
    // get the text from the request body and validate it
    let text = req.body.text;
    let lang = req.body.lang;

    // validate data received
    req.checkBody('text', 'Text is required.').notEmpty();
    req.checkBody('lang', 'Language is required.').notEmpty();
    req.checkBody('lang', 'Language must be a valid language.').isIn(req.app.get('upload').languages);

    req.getValidationResult().then((result) => {
        if (result.isEmpty()) {
            // perform validation
            req.app.get('validation').validateText(text, lang, req.user).then(resObj => {
                if (req.user) {
                    // if user logged in, get this validation's report
                    req.app.get('reports').getSingleReport(resObj.report.id).then(report => {
                        // add report to reports saved in session
                        req.session.userReports.splice(0, 0, report);
                        // regenerate user's medal to take this validation into account
                        req.session.userMedal = req.app.get('medals').generateUserMedal(req.session.userReports);
                        res.send(resObj);
                    });
                } else {
                    // if not logged in, just send the response
                    res.send(resObj);
                }
            });
        } else {
            res.status(400).send({
                errors: result.array()
            });
        }
    });
});

// route for file validation - accepts upload of a single file
router.post('/file', uploadFilter.fileUpload.single('file'), (req, res, next) => {
    let file = req.file;
    let lang = req.body.lang || null;

    // validate lang
    req.check('lang', 'Language must be a valid language.').optional().isIn(req.app.get('upload').languages);

    req.getValidationResult().then((result) => {
        if (result.isEmpty() && file) {
            // perform validation
            req.app.get('validation').validateFile(file.path, lang, req.user).then(resObj => {
                if (req.user) {
                    // if user logged in, get the report for this validation
                    req.app.get('reports').getSingleReport(resObj.report.id).then(report => {
                        // add to user's reports in session, regenerate medal, and send response
                        req.session.userReports.splice(0, 0, report);
                        req.session.userMedal = req.app.get('medals').generateUserMedal(req.session.userReports);
                        res.send(resObj);
                    });
                } else {
                    // if not logged in, just send response
                    res.send(resObj);
                }
            });
        } else {
            // if result empty, that means no file was uploaded
            if (result.isEmpty()) {
                res.status(400).send({
                    errors: [{
                        msg: 'A valid file is required.'
                    }]
                });
            } else {
                // if result not empty, show validation errors and delete the uploaded file
                req.app.get('upload').fileCleanup(file);
                res.status(400).send({
                    errors: result.array()
                });
            }
        }
    });

});

// route for github repo validation
router.post('/repo', userFilter.ensureLoggedIn(), (req, res, next) => {
    let repoName = req.body.repoName;

    // check for repo name in req body
    req.checkBody('repoName', 'Repo name is required.').notEmpty();

    // validate request
    req.getValidationResult().then((result) => {
        if (result.isEmpty()) {
            // perform repo validation
            req.app.get('validation').validateRepo(req.user, repoName).then(resObj => {
                if (req.user) {
                    // if user logged in, get the report for this validation
                    req.app.get('reports').getSingleReport(resObj.report.id).then(report => {
                        // add to user's reports in session, regenerate medal, and send response
                        req.session.userReports.splice(0, 0, report);
                        req.session.userMedal = req.app.get('medals').generateUserMedal(req.session.userReports);
                        res.send(resObj);
                    });
                } else {
                    // if not logged in, just send response
                    res.send(resObj);
                }
            }).catch(err => {
                res.status(err.status).send({
                    errors: [{
                        msg: err.msg
                    }]
                });
            });
        } else {
            // return an error
            res.status(400).send({
                errors: [{
                    msg: 'A valid repo name is required.'
                }]
            });
        }
    });
});

// route for zip validation that accepts a single file upload
router.post('/zip', uploadFilter.zipUpload.single('zip'), (req, res, next) => {
    let zip = req.file;

    if (zip) {
        req.app.get('validation').validateZip(req.user, zip.path).then(resObj => {
            if (req.user) {
                // if user logged in, get the report for this validation
                req.app.get('reports').getSingleReport(resObj.report.id).then(report => {
                    // add to user's reports in session, regenerate medal, and send response
                    req.session.userReports.splice(0, 0, report);
                    req.session.userMedal = req.app.get('medals').generateUserMedal(req.session.userReports);
                    res.send(resObj);
                });
            } else {
                // if not logged in, just send response
                res.send(resObj);
            }
        }).catch(err => {
            res.status(err.status).send({
                errors: [{
                    msg: err.msg
                }]
            });
        });
    } else {
        // return an error
        res.status(400).send({
            errors: [{
                msg: 'A valid zip is required.'
            }]
        });
    }
});

module.exports = router;
