/**
 * Service to perform various GitHub API requests.
 * @module gh
 */

const unirest = require('unirest');

/**
 * Builder for GitHub API requests.
 */
class GithubRequestBuilder {

    /**
     * Reset the builder on creation.
     */
    constructor() {
        this.reset();
    }

    /**
     * Reset to be able to perform a new request.
     */
    reset() {
        // start of API URL
        this.url = 'https://api.github.com/';
        // default headers
        this.headers = {
            'charset': 'utf-8',
            'user-agent': 'quayapp.com'
        };
        this.query = {};
        this.body = {};
        // default encoding
        this.encoding = 'utf-8';
    }

    /**
     * Append some path to URL to direct the request somewhere.
     * @param {string} path - the path to append to URL
     */
    addToUrl(path) {
        this.url += path;
    }

    /**
     * Add a new header to the request.
     * @param {string} key - the header key 
     * @param {string} value - the header value
     */
    addHeader(key, value) {
        this.headers[key] = value;
    }

    /**
     * Add a key/value pair to the request body.
     * @param {string} key - the key
     * @param {*} value - the value
     */
    addToBody(key, value) {
        this.body[key] = value;
    }

    /**
     * Set an authorization token in the request headers.
     * @param {string} token - the authorization token
     */
    setAuthorizationToken(token) {
        this.headers.Authorization = token;
    }

    /**
     * Add some query parameter.
     * @param {string} key - the key
     * @param {string} value - the value
     */
    addQuery(key, value) {
        this.query[key] = value;
    }

    /**
     * Add a user's access token to request in order to perform an API
     * request on their behalf.
     * @param {string} token - the access token
     */
    setAccessToken(token) {
        this.query.access_token = token;
    }

    /**
     * Change the request encoding (default is UTF-8)
     * @param {string} encoding - the encoding to set
     */
    setEncoding(encoding) {
        this.encoding = encoding;
    }

    /**
     * Perform an API request based on the builder configuration and return a Promise that resolves 
     * to the response body.
     * @param {boolean} isPost - true if this should be a POST request, false otherwise
     * @returns {Promise} resolves to API response body
     */
    makeRequest(isPost = false) {
        // set request to GET or POST based on isPost argument
        let request = isPost ? 'post' : 'get';
        let promise = new Promise((fulfill, reject) => {
            unirest[request](this.url)
                .type('json')
                .headers(this.headers)
                .query(this.query)
                .encoding(this.encoding)
                .send(this.body)
                .end(res => {
                    fulfill(res.body);
                });
        });
        // reset and return the promise
        this.reset();
        return promise;
    }

}

/**
 * The GitHub Service class. Allows performing various GitHub API requests.
 */
module.exports = class {

    /**
     * Creates a new GitHubRequestBuilder to use for building API requests.
     */
    constructor() {
        this.builder = new GithubRequestBuilder();
    }

    /**
     * Create a status on a GitHub commit.
     * @param {int} repoId - id of the GitHub repository
     * @param {string} sha - the sha of the commit to set a status on
     * @param {string} authorizationToken - integration authorization token that allows performing this request
     * @param {string} status - the status to set on the commit 
     * @param {string} url - url that GitHub user should be redirected to in order to view info about the status
     * @returns {Promise} Promise that resolves to GitHub API response body 
     */
    createStatus(repoId, sha, authorizationToken, status, url) {
        let path = `repositories/${repoId}/statuses/${sha}`;
        this.builder.addToUrl(path);
        this.builder.setAuthorizationToken(`token ${authorizationToken}`);
        this.builder.addToBody('state', status);
        this.builder.addToBody('target_url', url);
        return this.builder.makeRequest(true);
    }

    /**
     * Get an authorization token for a GitHub installation.
     * @param {id} installationId - intallation id 
     * @param {string} jwtToken - signed JWT token to send to GitHub
     * @returns {Promise} Promise that resolves to GitHub API response body
     */
    getInstallationToken(installationId, jwtToken) {
        let path = `installations/${installationId}/access_tokens`;
        this.builder.addToUrl(path);
        this.builder.addHeader('Accept', 'application/vnd.github.machine-man-preview+json');
        this.builder.setAuthorizationToken(`Bearer ${jwtToken}`);
        return this.builder.makeRequest(true);
    }

    /**
     * Download a GitHub repository zip. The request can be authorized either through a 
     * access token (to authenticate as repo owner) or an installation authorization token (to
     * authenticate as a GitHub installation).
     * @param {string} username - GitHub username of the repo owner
     * @param {string} repoName - name of the repository
     * @param {string} accessToken - access token to authenticate as repo owner
     * @param {string} authorizationToken - authorization token to authenticate as GitHub installation
     * @returns {Promise} Promise that resolves to GitHub API response body
     */
    downloadRepoZip(username, repoName, accessToken = null, authorizationToken = null) {
        if (!accessToken && !authorizationToken) {
            throw new Error('Either an access token or an installation authorization token must be supplied.');
        }
        let path = `repos/${username}/${repoName}/zipball`;
        this.builder.addToUrl(path);
        if (authorizationToken) {
            this.builder.setAuthorizationToken(`token ${authorizationToken}`);
        } else if (accessToken) {
            this.builder.setAccessToken(accessToken);
        }
        this.builder.setEncoding(null);
        return this.builder.makeRequest();
    }

    /**
     * Get a list of a user's GitHub repositories. 
     * @param {string} username - GitHub username 
     * @param {string} accessToken - access token to authenticate request for the user
     * @returns {Promise} Promise that resolves to GitHub API response body
     */
    getUserRepos(username, accessToken) {
        let path = `users/${username}/repos`;
        this.builder.addToUrl(path);
        this.builder.setAccessToken(accessToken);
        return this.builder.makeRequest();
    }

    /**
     * Get a repo tree for a GitHub repository.
     * @param {string} username - user's GitHub username
     * @param {string} repoName - repository name
     * @param {string} accessToken - access token to authenticate the request for the user
     * @returns {Promise} Promise that resolves to GitHub API response body
     */
    getRepoTree(username, repoName, accessToken) {
        let path = `repos/${username}/${repoName}/git/trees/master`;
        this.builder.addToUrl(path);
        this.builder.setAccessToken(accessToken);
        return this.builder.makeRequest();
    }

    /**
     * Get the contents of a single file in a GitHub repository. 
     * @param {string} username - user's GitHub username
     * @param {string} repoName - repository name
     * @param {string} filePath - path of the file in the repository
     * @param {string} accessToken - access token to authenticate the request for the user
     * @returns {Promise} Promise that resolves to GitHub API response body
     */
    getFileContents(username, repoName, filePath, accessToken) {
        let path = `repos/${username}/${repoName}/contents/${filePath}`;
        this.builder.addToUrl(path);
        this.builder.setAccessToken(accessToken);
        return this.builder.makeRequest();
    }
}
