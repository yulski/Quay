/**
 * Service for generating medals.
 * @module medals
 */

const Report = require('../models').Report;
const Message = require('../models').Message;

/**
 * MedalsService to generate medals for validations and users.
 */
module.exports = class {

    /**
     * Empty constructor.
     */
    constructor() {}

    /**
     * Generate a medal based on the number of errors.
     * @param {int} numErrors - number of errors 
     */
    generateMedal(numErrors) {
        // 0 -> GOLD, 1 -> SILVER, anything else -> BRONZE
        if (numErrors === 0) {
            return 'GOLD';
        } else if (numErrors === 1) {
            return 'SILVER';
        } else {
            return 'BRONZE';
        }
    }

    /**
     * Generates a medal for a single validation.
     * @param {Object} valRes - a validation response object
     */
    generateValidationMedal(valRes) {
        let numErrors = 0;
        if (valRes.messages) {
            // get number of error messages in the valRes
            numErrors = valRes.messages.filter(msg => msg.type === 'error').length;
        }
        return this.generateMedal(numErrors);
    }

    /**
     * Generates a medal for a user based on their validation reports.
     * @param {Object[]} userReports - array of the user's reports
     */
    generateUserMedal(userReports) {
        if (userReports && userReports.length) {
            let totalErrors = 0;
            for (let report of userReports) {
                if (report.messages) {
                    // get number of error messages in the report
                    totalErrors += report.messages.filter(msg => msg.type === 'error').length;
                }
            }
            // get average errors per report & generate medal based on this average
            let avg = Math.round(totalErrors / userReports.length);
            return this.generateMedal(avg);
        } else {
            // default to -1 (will generate BRONZE atm)
            return this.generateMedal(-1);
        }
    }

};
