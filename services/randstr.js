/**
 * Service for generating and working with random strings for use in AccessKeys.
 * @module randstr
 */

const randomstring = require('randomstring');
const bcrypt = require('bcrypt');

const AccessKey = require('../models').AccessKey;

/**
 * RandstrService to work with random strings. There random strings are used for access keys.
 */
module.exports = class {

    /**
     * Empty constructor.
     */
    constructor() {}

    /**
     * Generate a unique random string. Unique means that no access key of type [type] currently in the 
     * database is using it.
     * @param {string} type - the type of the access key to generate a unique randstr for 
     * @returns {Promise} Promise that resolves to a unique random string
     */
    generateUniqueRandStr(type) {
        return new Promise((fulfill, reject) => {
            // generate a random alphanumeric string
            let randStr = randomstring.generate({
                charset: 'alphanumeric'
            });
            // check if the string is unique
            this.isRandStrUnique(randStr, type).then((unique) => {
                // if it is unique, return it
                if (unique) {
                    fulfill(randStr);
                } else {
                    // if not unique, call this function again
                    fulfill(this.generateUniqueRandStr(type));
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }

    /**
     * Checks if a random string is unique for a particular type of access key.
     * @param {string} randStr - the random string to check uniqueness off
     * @param {string} type - type of access key to check
     * @returns {Promise} a Promise that resolves to true if the random string is unique or false otherwise
     */
    isRandStrUnique(randStr, type) {
        return new Promise((fulfill, reject) => {
            AccessKey.findAll({
                where: {
                    type: type
                }
            }).then((keys) => {
                if (keys.length === 0) {
                    fulfill(true);
                }
                let loops = 0;
                let breakLoop = false;
                for (let key of keys) {
                    if (breakLoop) {
                        break;
                    }
                    bcrypt.compare(randStr, key.hash, (err, same) => {
                        loops++;
                        if (!err) {
                            // if match found
                            if (same) {
                                breakLoop = true;
                                // false - randStr is not unique
                                fulfill(false);
                            } else if (loops === keys.length) {
                                breakLoop = true;
                                // true - randStr is unique
                                fulfill(true);
                            }
                        } else {
                            reject('Bcrypt error');
                        }
                    });
                }
            });
        });
    }

}
