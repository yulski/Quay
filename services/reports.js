/**
 * Report service for working with validation reports.
 * @module reports
 */

const fecha = require('fecha');
const fs = require('fs');

const Message = require('../models').Message;
const Report = require('../models').Report;
const ReportMessage = require('../models').ReportMessage;

/**
 * ReportService for working with validation reports.
 */
module.exports = class {

    /**
     * Create a new ReportService with a MedalService to work with medals.
     * @param {MedalService} medalService - a medal service instance 
     */
    constructor(medalService) {
        this.medalService = medalService;
    }

    /**
     * Create a report for a user based on a validation response.
     * @param {Object} valRes - a validation response object
     * @param {User} user - the User entity to create the report for
     * @returns {Promise} a Promise that resolves to the created report
     */
    createReport(valRes, user) {
        let date = fecha.format(new Date(), 'YYYY-MM-DD hh:mm:ss');

        // create medal for the validation
        let medal = this.medalService.generateValidationMedal(valRes);

        return new Promise((fulfill, reject) => {
            // create new Report
            Report.create({
                date: date,
                user: user.id,
                medal: medal
            }).then(report => {
                let loops = 0;
                if (valRes.messages.length) {
                    for (let message of valRes.messages) {
                        // save all messages and create ReportMessages to associate them with the report
                        message.save().then(() => {
                            ReportMessage.create({
                                report: report.id,
                                message: message.id
                            }).then(reportMessage => {
                                loops++;
                                if (loops === valRes.messages.length) {
                                    fulfill(report);
                                }
                            });
                        });
                    }
                } else {
                    fulfill(report);
                }
            });
        });
    }

    /**
     * Get all validation reports of a particular user.
     * @param {User} user - the User whose reports will be fetched 
     * @returns {Promise} a Promise that resolves to an array of the user's reports
     */
    getUserReports(user) {
        return new Promise((fulfill, reject) => {
            // get all of the user's reports
            Report.findAll({
                where: {
                    user: user.id
                }
            }).then(userReports => {
                // return null if no reports
                if (userReports.length === 0) {
                    fulfill(null);
                } else {
                    // get all reports first before sorting them. if they are sorted as they are 
                    // asynchronously fetched, all the ones with no messages return quicker and this 
                    // messess up the sorting order
                    new Promise((fulfill, reject) => {
                        let loops = 0;
                        let reports = [];
                        for (let report of userReports) {
                            // get each report w/ messages etc.
                            this.getSingleReport(report.id).then(currReport => {
                                reports.push(currReport);
                                loops++;
                                if (loops === userReports.length) {
                                    fulfill(reports);
                                }
                            });
                        }
                    }).then(reports => {
                        // once all reports are fetched, sort and return them
                        fulfill(this.sortReports(reports));
                    });
                }
            });
        });
    }

    /**
     * Get a single report by id. This is not a Report entity, but an object with an array of 
     * Messages, date, medal, user, and id.
     * @param {int} id - the id of the report
     * @returns {Promise} a Promise that resolves to the report object
     */
    getSingleReport(id) {
        return new Promise((fulfill, reject) => {
            // find the Report entity
            Report.findById(id).then(report => {
                if (report) {
                    // find all ReportMessages for the report
                    ReportMessage.findAll({
                        where: {
                            report: report.id
                        }
                    }).then(reportMessages => {
                        let reportObj = {
                            id: report.id,
                            date: report.date,
                            user: report.user,
                            medal: report.medal,
                            messages: []
                        };
                        // if the report has messages associtated with it, fetch them all
                        if (reportMessages.length) {
                            let loops = 0;
                            for (let reportMessage of reportMessages) {
                                // get each message and add to messages array
                                Message.findById(reportMessage.message).then(message => {
                                    reportObj.messages.push(message);
                                    loops++;
                                    if (loops === reportMessages.length) {
                                        fulfill(reportObj);
                                    }
                                });
                            }
                        } else {
                            // if no messages, just return the basic reportObj
                            fulfill(reportObj);
                        }
                    });
                } else {
                    fulfill(null);
                }
            });
        });
    }

    /**
     * Sorts an array of reports by id.(This effectively sorts them by date too). The algorithm
     * used is merge sort.
     * @param {Object[]} reports - array of reports
     * @returns {Object[]} the reports passed to it but sorted by id
     */
    sortReports(reports) {
        // merge function - merge left and right arr
        let merge = (left, right) => {
            let res = [];

            while(left.length && right.length) {
                if(left[0].id > right[0].id) {
                    res.push(left.shift());
                } else {
                    res.push(right.shift());
                }
            }

            while(left.length) {
                res.push(left.shift());
            }

            while(right.length) {
                res.push(right.shift());
            }

            return res;
        };
        
        // sort func - split in two
        let sort = (reports) => {
            if(reports.length < 2) {
                return reports;
            }
            let middle = parseInt(reports.length / 2);
            let left = reports.slice(0, middle);
            let right = reports.slice(middle);
            return merge(sort(left), sort(right));
        };

        return sort(reports);
    }

};
