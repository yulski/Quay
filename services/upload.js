/**
 * Upload service. Different from the upoad filter in that this module's functionality can be used inside
 * request bodies.
 * @module upload
 */

const path = require('path');

const fh = require('../src/file-handling');

/**
 * UploadService class. A helper class for working with file uploads.
 */
module.exports = class {

    /**
     * Constructor creates outDir (same as outDir in upload filter) and an array of 
     * allowed languages (the languages allowed for validation).
     */
    constructor() {
        this.languages = ['html', 'css'];
        this.outDir = path.join(__dirname, '..', 'temp');
    }

    /**
     * Cleans up after a file upload. Deletes the file found at the path passed to it.
     * @param {string} file - file path
     */
    fileCleanup(file) {
        fh.deleteFile(file).then((del) => {
            if (!del) {
                console.log('ERROR deleting file');
            }
        });
    }

    /**
     * Cleans up after a zip upload. Deletes the uploaded zip file and the directory where 
     * the zip was extracted.
     * @param {strin} zip - zip path
     * @param {*} dir - directory the zip was extracted to
     */
    zipCleanup(zip, dir) {
        fh.deleteFile(zip).then((del) => {
            if (!del) {
                console.log('ERROR deleting zip');
            }
        });
        fh.deleteDirectory(dir).then((del) => {
            if (!del) {
                console.log('ERROR deleting directory');
            }
        });
    }

    /**
     * Generates name for an uploaded file.
     * @param {string} fileName - original name of the file
     */
    generateFileName(fileName) {
        let arr = fileName.split('.');
        let ext = arr.pop();
        let name = arr.join('.') + '-' + Date.now() + '.' + ext;
        return name;
    }

};
