/**
 * Module through which validation should be performed.
 * @module validation
 */

const path = require('path');
const fs = require('fs');
const fecha = require('fecha');
const heck = require('@julek14/heck');

const validation = require('../src/validation');
const fh = require('../src/file-handling');
const normalize = require('../src/normalize');

/**
 * ValidationService to handle validation.
 */
module.exports = class {

    /**
     * Create a new ValidationService.
     * @param {MedalService} medalService - a MedalService that the ValidationService should use
     * @param {GitHubService} ghService - a GitHubService that the ValidationService should use
     * @param {ReportService} reportService - a ReportService that the ValidationService should use
     * @param {UploadService} uploadService - a UploadService that the ValidationService should use
     */
    constructor(medalService, ghService, reportService, uploadService) {
        this.medalService = medalService;
        this.ghService = ghService;
        this.reportService = reportService;
        this.uploadService = uploadService;
    }

    /**
     * Perform text validation.
     * @param {string} text - the text content to validate
     * @param {string} lang - the language the text is in
     * @param {User} user - the User who is performing the validation
     * @returns {Promise} a Promise that resolves to an object with the keys 'valRes' and 'report'.
     * 'valRes' is the validation response object. 'report' is the report generated for the validation, or an object 
     * with key 'medal' which holds the medal generated for this validation if no user was passed.
     */
    validateText(text, lang, user) {
        return new Promise((fulfill, reject) => {
            // validate text through src/validation module and Heck
            // this is concurrent maybe
            new Promise((fulfill, reject) => {
                let notDone = 2;
                let res = {
                    messages: []
                };
                validation.validateText(text, lang).then(valRes => {
                    res.messages = res.messages.concat(valRes.messages);
                    notDone--;
                    if (notDone === 0) {
                        fulfill(res);
                    }
                });
                heck(text).then(heckRes => {
                    let normalizedResponse = normalize(heckRes, 'heck');
                    res.messages = res.messages.concat(normalizedResponse.messages);
                    notDone--;
                    if (notDone === 0) {
                        fulfill(res);
                    }
                });
            }).then(valRes => {
                // once validation is done, create report if user logged in
                if (user) {
                    this.reportService.createReport(valRes, user).then(report => {
                        fulfill({
                            valRes: valRes,
                            report: report
                        });
                    });
                } else {
                    // if no user, generate medal and return in report object
                    let medal = this.medalService.generateValidationMedal(valRes);
                    fulfill({
                        valRes: valRes,
                        report: {
                            medal: medal
                        }
                    });
                }
            });
        });
    }

    /**
     * Perform file validation.
     * @param {string} file - the path of the file to validate
     * @param {string} lang - the language the file is in
     * @param {User} user - the User who is performing the validation
     * @returns {Promise} a Promise that resolves to an object with the keys 'valRes' and 'report'.
     * 'valRes' is the validation response object. 'report' is the report generated for the validation, or an object 
     * with key 'medal' which holds the medal generated for this validation if no user was passed.
     */
    validateFile(file, lang, user) {

        return new Promise((fulfill, reject) => {

            // validate through src/validation and Heck (?concurrently?)
            new Promise((fulfill, reject) => {
                let notDone = 2;
                let res = {
                    messages: []
                };
                validation.validateFile(file, lang).then(valRes => {
                    res.messages = res.messages.concat(valRes.messages);
                    notDone--;
                    if (notDone === 0) {
                        fulfill(res);
                    }
                });
                // read file and pass content to Heck
                fs.readFile(file, {
                    encoding: 'utf-8'
                }, (err, data) => {
                    if (err) {
                        console.error(err);
                    }
                    heck(data.toString()).then(heckRes => {
                        // normalize Heck response
                        let normalizedResponse = normalize(heckRes, 'heck');
                        res.messages = res.messages.concat(normalizedResponse.messages);
                        notDone--;
                        if (notDone === 0) {
                            fulfill(res);
                        }
                    });
                });
            }).then(valRes => {
                // once validation is done, create report if user logged in
                if (user) {
                    this.reportService.createReport(valRes, user).then(report => {
                        fulfill({
                            valRes: valRes,
                            report: report
                        });
                    });
                } else {
                    // if no user, generate medal and return in report object
                    let medal = this.medalService.generateValidationMedal(valRes);
                    fulfill({
                        valRes: valRes,
                        report: {
                            medal: medal
                        }
                    });
                }
            });
        });
    }

    /**
     * Perform GitHub repo validation.
     * @param {User} user - the User who is performing the validation
     * @param {string} repoName - name of the repo to validate
     * @param {string} authorizationToken - optional authorization token to perform GitHub API requests
     * as an installation rather than as a user
     * @returns {Promise} a Promise that resolves to an object with the keys 'valRes' and 'report'.
     * 'valRes' is the validation response object. 'report' is the report generated for the validation, or an object 
     * with key 'medal' which holds the medal generated for this validation if no user was passed.
     */
    validateRepo(user, repoName, authorizationToken = null) {
        return new Promise((fulfill, reject) => {
            // download the repo zip
            this.ghService.downloadRepoZip(user.ghuname, repoName, user.ghtoken, authorizationToken).then(repoZip => {
                let zipName = this.uploadService.generateFileName(`${user.ghuname}-${repoName}.zip`);
                let zipPath = path.join(this.uploadService.outDir, zipName);
                // write the zip
                let writeStream = fs.createWriteStream(zipPath);
                writeStream.on('finish', () => {
                    // once the repo zip is downloaded, let the 'validateZip' function handle it
                    this.validateZip(user, zipPath).then(valRes => {
                        fulfill(valRes);
                    }).catch(err => {
                        reject(err);
                    });
                });
                writeStream.write(repoZip);
                writeStream.end();
            });
        });
    }

    /**
     * Validate the contents of a zip archive.
     * @param {User} user - the User who is performing the validation
     * @param {string} zip - the path of the zip file to validate
     * @returns {Promise} a Promise that resolves to an object with the keys 'valRes' and 'report'.
     * 'valRes' is the validation response object. 'report' is the report generated for the validation, or an object 
     * with key 'medal' which holds the medal generated for this validation if no user was passed.
     */
    validateZip(user, zip) {
        return new Promise((fulfill, reject) => {
            // extract the zip
            fh.extractArchive(zip).then(extrDir => {
                // find all files inside the extracted folder
                fh.findFiles(extrDir).then(files => {
                    // filter files to only get files valid for validation
                    let filteredFiles = files.filter(file => {
                        return fh.checkFileExtension(file);
                    });
                    // if there are no valid files, reject
                    if (filteredFiles.length === 0) {
                        this.uploadService.zipCleanup(zip, extrDir);
                        reject({
                            status: 400,
                            msg: 'none of the zip files in the zip are accepted for validation'
                        });
                    } else {
                        // if there are valid files, validate the files

                        // validate file list using validation module and Heck
                        // this is concurrent maybe
                        new Promise((fulfill, reject) => {
                            let notDone = 2;
                            let res = {
                                messages: []
                            };
                            let dirFiles = filteredFiles.map(file => {
                                return path.join(extrDir, file);
                            });
                            validation.validateFileList(dirFiles).then(valRes => {
                                // concat validation messages to have a single messages array
                                valRes = valRes.reduce((acc, curr) => {
                                    acc.messages = acc.messages.concat(curr.messages);
                                    return acc;
                                }, {
                                    messages: []
                                });
                                res.messages = res.messages.concat(valRes.messages);
                                notDone--;
                                if (notDone === 0) {
                                    this.uploadService.zipCleanup(zip, extrDir);
                                    fulfill(res);
                                }
                            });
                            heck(extrDir, 'directory').then(heckRes => {
                                // normalize Heck response
                                let normalizedResponse = normalize(heckRes, 'heck');
                                res.messages = res.messages.concat(normalizedResponse.messages);
                                notDone--;
                                if (notDone === 0) {
                                    this.uploadService.zipCleanup(zip, extrDir);
                                    fulfill(res);
                                }
                            });
                        }).then(valRes => {
                            // once validation is done, create report if user logged in
                            if (user) {
                                this.reportService.createReport(valRes, user).then(report => {
                                    fulfill({
                                        valRes: valRes,
                                        report: report
                                    });
                                });
                            } else {
                                // if no user, generate medal and return in report object
                                let medal = this.medalService.generateValidationMedal(valRes);
                                fulfill({
                                    valRes: valRes,
                                    report: {
                                        medal: medal
                                    }
                                });
                            }
                        });
                    }
                });
            });
        });
    }

};
