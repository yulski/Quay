const fh = require('../src/file-handling');

const path = require('path');
const fs = require('fs');
const copyDir = require('copy-dir');

describe('Test file extension checking', () => {

    let validFile1 = path.join(__dirname, 'test-data', 'file.html');
    let validFile2 = path.join(__dirname, 'test-data', 'file2.htm');
    let invalidFile = path.join(__dirname, 'test-data', 'file.md');

    it('should return true on valid extensions', () => {
        let res1 = fh.checkFileExtension(validFile1);
        let res2 = fh.checkFileExtension(validFile2);
        expect(res1).toBe(true);
        expect(res2).toBe(true);
    });

    it('should return false on invalid extension', () => {
        let res = fh.checkFileExtension(invalidFile);
        expect(res).toBe(false);
    });

});

describe('Test archive extension checking', () => {

    let validArchive = path.join(__dirname, 'test-data', 'archive1.zip');
    let invalidArchive = path.join(__dirname, 'test-data', 'file.md');

    it('should return true on valid archive extension', () => {
        let res = fh.checkArchiveExtension(validArchive);
        expect(res).toBe(true);
    });

    it('should return false on invalid archive extension', () => {
        let res = fh.checkArchiveExtension(invalidArchive);
        expect(res).toBe(false);
    });

});

describe('Test deleting a directory', () => {

    let dirname = 'del-dir-test';
    let copiedDir = path.join(__dirname, 'test-data', dirname);
    let dir = path.join(__dirname, 'test-data', 'dir');

    beforeEach(() => {
        copyDir.sync(dir, copiedDir);
    });

    it('should delete directory', (done) => {
        fh.deleteDirectory(copiedDir).then((res) => {
            expect(res).toBe(true);
            let exists = fs.existsSync(copiedDir);
            expect(exists).toBe(false);
            done();
        });
    });
});

describe('Test deleting a list of files', () => {

    let fileList = [
        path.join(__dirname, 'test-data', 'a_file.html'), path.join(__dirname, 'test-data', 'another_file.html')
    ];

    beforeEach(() => {
        for (let file of fileList) {
            let f = fs.openSync(file, 'w');
            fs.closeSync(f);
        }
    });

    it('should delete all files passed to it', (done) => {
        fh.deleteFileList(fileList).then((res) => {
            expect(res).toBe(true);
            for (let file of fileList) {
                try {
                    fs.openSync(file, 'r');
                    fail('File was not deleted');
                } catch (ex) {
                    expect(ex.code).toBe('ENOENT');
                }
            }
            done();
        });
    });

});


describe('Test extracting zip file archives', () => {

    let archive = path.join(__dirname, 'test-data', 'archive1.zip');
    let outDir = path.join(__dirname, 'test-data');
    let dir = '';

    afterEach(() => {
        if (dir) {
            let arr = fs.readdirSync(dir);
            for (let f of arr) {
                fs.unlinkSync(path.join(dir, f));
            }
            fs.rmdirSync(dir);
        }
    });

    it('should extract files into a directory', (done) => {
        fh.extractArchive(archive, outDir).then((res) => {
            dir = res;
            expect(typeof dir).toBe('string');
            let arr = dir.split(path.sep);
            arr.pop();
            expect(arr.join(path.sep)).toBe(outDir);
            done();
        });
    });

});


describe('Test fileExists function', () => {

    let file = path.join(__dirname, 'test-data', 'file.html');
    let archive = path.join(__dirname, 'test-data', 'archive1.zip');
    let noFile = path.join(__dirname, 'test-data', 'xyz.html');

    it('should return a Promise', () => {
        let res = fh.fileExists(file);
        expect(res instanceof Promise).toBe(true);
    });

    it('should return true for existing file', (done) => {
        fh.fileExists(file).then((exists) => {
            expect(exists).toBe(true);
            done();
        });
    });

    it('should return true for existing archive', (done) => {
        fh.fileExists(archive).then((exists) => {
            expect(exists).toBe(true);
            done();
        });
    });

    it('should return false for non-existent archive', (done) => {
        fh.fileExists(noFile).then((exists) => {
            expect(exists).toBe(false);
            done();
        });
    });

});

describe('Test finding all files in a dirctory', () => {
    let dir = path.join(__dirname, 'test-data', 'dir');
    let files = [path.join('dir2', 'file2.html'), path.join('dir3', 'file3.html'), path.join('dir3', 'dir4', 'file4.html')];

    it('should return correct list of files', (done) => {
        fh.findFiles(dir).then((foundFiles) => {
            expect(foundFiles instanceof Array).toBe(true);
            expect(foundFiles.length).toBe(3);
            expect(foundFiles).toEqual(files);
            done();
        });
    });
});

describe('Test getting file contents', () => {

    let file = path.join(__dirname, 'test-data', 'file.html');
    let noFile = path.join(__dirname, 'test-data', 'xyz.html');
    let contents = fs.readFileSync(file, {
        encoding: 'utf-8'
    });

    it('should return correct file contents', (done) => {
        fh.getFileContents(file).then((res) => {
            expect(res).toEqual(contents);
            done();
        });
    });

    it('should reject Promise if file does not exist', (done) => {
        fh.getFileContents(noFile).then((res) => {
            fail('did not reject Promise with non-existant file');
            done();
        }).catch((err) => {
            expect(err.code).toBe('ENOENT');
            done();
        });
    });

});

describe('test getting file type', () => {
    let htmlFile1 = path.join(__dirname, 'test-data', 'file1.html');
    let htmlFile2 = path.join(__dirname, 'test-data', 'file2.htm');
    let cssFile = path.join(__dirname, 'test-data', 'file-warn.css');
    let noFile = path.join(__dirname, 'test-data', 'fake-file.zzz');

    it('should return "html" for a html file', () => {
        let res1 = fh.getFileType(htmlFile1);
        expect(res1).toBe('html');
        let res2 = fh.getFileType(htmlFile2);
        expect(res2).toBe('html');
    });

    it('should return "css" for a css file', () => {
        let res = fh.getFileType(cssFile);
        expect(res).toBe('css');
    });

    it('should return empty string for unidentified file', () => {
        let res = fh.getFileType(noFile);
        expect(res).toBe('');
    });
});

describe('test isArchiveOk function', () => {

    let archive = path.join(__dirname, 'test-data', 'archive1.zip');
    let invalidArchive = path.join(__dirname, 'test-data', 'file.md');
    let noArchive = path.join(__dirname, 'test-data', 'xyz.zip');

    it('should return true with valid archive', (done) => {
        fh.isArchiveOk(archive).then((res) => {
            expect(res.ok).toBe(true);
            expect(res.message).toBe('ok');
            done();
        });
    });

    it('should return false when no archive passed', (done) => {
        fh.isArchiveOk().then((res) => {
            expect(res.ok).toBe(false);
            expect(res.message).toBe('No archive passed to function');
            done();
        });
    });

    it('should return false when passed archive with invalid extension', (done) => {
        fh.isArchiveOk(invalidArchive).then((res) => {
            expect(res.ok).toBe(false);
            expect(res.message).toBe('The passed archive ends in an unaccepted extension');
            done();
        });
    });

    it('should return false if archive does not exist', (done) => {
        fh.isArchiveOk(noArchive).then((res) => {
            expect(res.ok).toBe(false);
            expect(res.message).toBe('The archive does not exist');
            done();
        });
    });

});

describe('Test isFileOk function', () => {

    let file = path.join(__dirname, 'test-data', 'file.html');
    let invalidFile = path.join(__dirname, 'test-data', 'file.md');
    let noFile = path.join(__dirname, 'test-data', 'xyz.html');

    it('should return true with valid file', (done) => {
        fh.isFileOk(file).then((res) => {
            expect(res.ok).toBe(true);
            expect(res.message).toBe('ok');
            done();
        });
    });

    it('should return false when no file passed', (done) => {
        fh.isFileOk().then((res) => {
            expect(res.ok).toBe(false);
            expect(res.message).toBe('No file passed to function');
            done();
        });
    });

    it('should return false when passed file with invalid extension', (done) => {
        fh.isFileOk(invalidFile).then((res) => {
            expect(res.ok).toBe(false);
            expect(res.message).toBe('The passed file ends in an unaccepted extension');
            done();
        });
    });

    it('should return false if file does not exist', (done) => {
        fh.isFileOk(noFile).then((res) => {
            expect(res.ok).toBe(false);
            expect(res.message).toBe('The file does not exist');
            done();
        });
    });

});