process.env.JAWSDB_URL = 'mysql://user:pass@u.r.l:3306/db'; // TODO change

const normalize = require('../src/normalize');

describe('test response normalization', () => {

    // This is a response as returned by the W3C HTML validation service
    const htmlResp = {
        messages: [
            {
                type: 'error',
                lastLine: 1,
                lastColumn: 0,
                firstColumn: 99,
                message: 'This is a sample message',
                extract: 'test',
                hiliteStart: 1,
                hiliteLength: 1
            }
        ]
    };

    // This is a response as returned by the W3C CSS validation service
    const cssResp = {
        errors: [
            {
                line: 1,
                level: 2,
                message: 'This is a sample message',
                type: 'sample-type',
                uri: 'file://localhost/Test'
            }
        ],
        warnings: []
    };

    it('should return basic object with messages array if no service name passed', () => {
        let normalized = normalize(htmlResp);
        expect(normalized instanceof Object).toBe(true);
        expect(normalized.hasOwnProperty('messages')).toBe(true);
        expect(normalized.messages instanceof Array).toBe(true);
        expect(normalized.messages.length).toBe(0);
    });

    it('should normalize HTML response', () => {
        let normalized = normalize(htmlResp, 'html');
        expect(normalized instanceof Object).toBe(true);
        expect(normalized.hasOwnProperty('messages')).toBe(true);
        expect(normalized.messages instanceof Array).toBe(true);
        let msg = normalized.messages[0];
        let htmlMsg = htmlResp.messages[0];
        expect(msg.type).toBe(htmlMsg.type);
        expect(msg.lastLine).toBe(htmlMsg.lastLine);
        expect(msg.message).toBe(htmlMsg.message);
        expect(msg.extract).toBe(htmlMsg.extract);
    });

    it('should normalize CSS response', () => {
        let normalized = normalize(cssResp, 'css');
        expect(normalized instanceof Object).toBe(true);
        expect(normalized.hasOwnProperty('messages')).toBe(true);
        expect(normalized.messages instanceof Array).toBe(true);
        let msg = normalized.messages[0];
        let cssMsg = cssResp.errors[0];
        expect(msg.type).toBe('error');
        expect(msg.lastLine).toBe(cssMsg.line);
        expect(msg.message).toBe(cssMsg.message);
        expect(msg.extract).toBe(null);
        expect(msg.otherInfo).toBe(cssMsg.type);
    });

});
