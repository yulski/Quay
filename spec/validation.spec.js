const validation = require('../src/validation');
const nock = require('nock');
const path = require('path');
const fs = require('fs');

describe('Testing basic text validation', () => {
    
    let requestText = '<!DOCTYPE html><html><head><title>title</title><meta charset="utf-8"></head><body>body</body></html>';

    let mockedResponseMessage = 'mocked response body';

    beforeEach(() => {
        nock.disableNetConnect();
        let scope = nock('http://validator.w3.org', {
            reqheaders: {
                'Content-Type': 'text/html',
                'charset': 'utf-8',
                'user-agent': 'validation.js'
            }
        })
        .post('/nu/', requestText)
        .query({out: 'json'})
        .reply(201, {
            ok: true,
            messages: [{message: mockedResponseMessage}]
        });
    });

    it('should return a Promise on empty input text', () => {
        let res = validation.validateText('');
        expect(res instanceof Promise).toBe(true);
    });

    it('should return a Promise that resolves to empty object on empty input text', (done) => {
        validation.validateText('').then((res) => {
            expect(res).toEqual({});
            done();
        });
    });

    it('should return a Promise when passed input text', () => {
        let res = validation.validateText(requestText, 'html');
        expect(res instanceof Promise).toBe(true);
    });

    it('should return Promise that resolves to mocked response message', (done) => {
        validation.validateText(requestText, 'html').then((res) => {
            expect(res.messages[0].message).toBe(mockedResponseMessage);
            done();
        });
    });

});

describe('Testing file validation', () => {
    let file = path.join(__dirname, 'test-data', 'file.html');
    let fileContent = fs.readFileSync(file).toString();
    let mockedResponseMessage = 'mocked response body';

    beforeEach(() => {
        nock.disableNetConnect();
        let scope = nock('http://validator.w3.org', {
            reqheaders: {
                'Content-Type': 'text/html',
                'charset': 'utf-8',
                'user-agent': 'validation.js'
            }
        })
        .post('/nu/', fileContent)
        .query({out: 'json'})
        .reply(201, {
            ok: true,
            messages: [{message: mockedResponseMessage}]
        });
    });

    it('should reject if no file name passed', (done) => {
        validation.validateFile().then((res) => {
            fail('validateFile did not reject with no file name');
            done();
        }).catch((err) => {
            expect(err).toBe('No file path passed to validation.js');
            done();
        });
    });

    it('should return a Promise if file name passed', () => {
        let res = validation.validateFile(file);
        expect(res instanceof Promise).toBe(true);
    });

    it('should return Promise that resolves to mocked response message', (done) => {
        validation.validateFile(file).then((res) => {
            expect(res.messages[0].message).toBe(mockedResponseMessage);
            done();
        });
    });

});

describe('Testing file list validation', () => {

    let file = path.join(__dirname, 'test-data', 'file.html');

    let fileContent = fs.readFileSync(file).toString();

    let fileList = [file, file];

    let mockedResponseMessage = 'mocked response body';

    let makeInterceptor = () => {
        return nock('http://validator.w3.org', {
            reqheaders: {
                'Content-Type': 'text/html',
                'charset': 'utf-8',
                'user-agent': 'validation.js'
            }
        })
        .post('/nu/', fileContent)
        .query({out: 'json'})
        .reply(201, {
            ok: true,
            messages: [{message: mockedResponseMessage}]
        });
    };

    beforeEach(() => {
        nock.disableNetConnect();
        let scope1 = makeInterceptor();
        let scope2 = makeInterceptor();
    });
    
    it('should throw error if no file list passed', (done) => {
        validation.validateFileList().then((res) => {
            fail('validateFileList did not reject with no file list');
            done();
        }).catch((err) => {
            expect(err).toBe('No file list passed to validation.js');
            done();
        });
    });

    it('should return a Promise if file list passed', () => {
        let res = validation.validateFileList(fileList);
        expect(res instanceof Promise).toBe(true);
    });

    it('should return an array of responses', (done) => {
        validation.validateFileList(fileList).then((res) => {
            expect(res instanceof Array).toBe(true);
            done();
        });
    });

    it('should return array of mocked responses', (done) => {
        validation.validateFileList(fileList).then((res) => {
            for(let response of res) {
                expect(response.messages[0].message).toBe(mockedResponseMessage);
            }
            done();
        });
    });

});

