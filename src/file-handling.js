/**
 * File handling module that takes care of all operations involving the file system
 * @module file-handling
 */

const fs = require('fs');
const path = require('path');

const unzip = require('unzip');

/**
 * Array of archive extensions that are accepted for validation.
 */
const acceptedArchiveExtensions = ['.zip'];

/**
 * Array of file extensions that are accepted for validation.
 */
const acceptedFileExtensions = ['.html', '.htm', '.css'];

/**
 * Path of directory to extract archives to
 */
const outDir = path.join(__dirname, '..', 'temp');

/**
 * Checks whether the archive passed has an extension that is accepted for validation.
 * @param {string} archivePath - the path of the archive whose extension is to be checked
 * @returns {boolean} true if archive extension is accepted for validation, false otherwise 
 */
function checkArchiveExtension (archivePath) {
    let extension = path.extname(archivePath);
    return acceptedArchiveExtensions.indexOf(extension) >= 0;
}

/**
 * Checks whether the file passed has an extension that is accepted for validation.
 * @param {string} filePath - the path of the file whose extension is to be checked
 * @returns {boolean} true if file extension is accepted for validation, false otherwise
 */
function checkFileExtension (filePath) {
    let extension = path.extname(filePath);
    return acceptedFileExtensions.indexOf(extension) >= 0;
}

/**
 * Deletes a directory tree (a directory, all its subdirectories and files).
 * @param {string} dir - path of the directory to delete
 * @returns {Promise} a Promise that resolves to a boolean value indicating if the directory was deleted successfully
 */
function deleteDirectory (dir) {
    let counter = 0;
    return new Promise((fulfill, reject) => {
        // read directory contents
        fs.readdir(dir, (err, files) => {
            if (files != null && files.length > 0) {
                // loop through directory contents
                for (let file of files) {
                    // get info on each file
                    fs.lstat(path.join(dir, file), (err, stats) => {
                        if (stats.isFile()) {
                            // if file, delete it
                            deleteFile(path.join(dir, file)).then((deleted) => {
                                counter++;
                                if (counter === files.length) {
                                    // if all files deleted, remove parent directory and return
                                    fs.rmdir(dir, (err) => {
                                        fulfill(!err);
                                    });
                                }
                            });
                        } else if (stats.isDirectory()) {
                            // if directory, recurse with it
                            deleteDirectory(path.join(dir, file)).then((deleted) => {
                                counter++;
                                if (counter === files.length) {
                                    // if all files deleted, remove parent directory and return
                                    fs.rmdir(dir, (err) => {
                                        fulfill(!err);
                                    });
                                }
                            });
                        }
                    });
                }
            } else {
                // if directory is empty, remove it and return
                fs.rmdir(dir, (err) => {
                    fulfill(!err);
                });
            }
        });
    });
}

/**
 * Deletes a single file.
 * @param {string} filePath - the path of the file to be deleted
 * @returns {Promise} - a Promise that resolves to `true` if the file was deleted successfully or `false` otherwise
 */
function deleteFile (filePath) {
    // returns a Promise
    return new Promise((fulfill, reject) => {
        fs.unlink(filePath, (err) => {
            // istanbul ignore next
            fulfill(!err);
        });
    });
}

/**
 * Deletes a list of files.
 * were deleted successfully or `false` otherwise.
 * @param {Array} - a string array of files to be deleted
 * @returns {Promise} a Promise that resolves to true if all files were deleted successfully 
 */
function deleteFileList (fileList) {
    let promises = [];
    for (let file of fileList) {
        promises.push(deleteFile(file));
    }
    // resolve all Promises and then reduce that array to a single value
    return Promise.all(promises).then((res) => res.reduce((total, curr) => total && curr));
}

/**
 * Extracts a zip archive to the output directory.
 * @param {string} archivePath - the path of the zip file that is to be extracted
 * @param {string=} outputPath - the path to extract the zip file to
 * @returns {string} an absolute path to the directory holding the extracted zip contents
 */
function extractArchive (archivePath, outputPath) {
    let chosenOutDir = outputPath || outDir;
    return new Promise((fulfill, reject) => {
        // make temporary directory to hold extracted files
        fs.mkdtemp(chosenOutDir + path.sep, (err, dir) => {
            // read archive and extract all files to temporary output directory
            fs.createReadStream(archivePath).pipe(unzip.Extract({
                path: dir
            })).on('close', () => {
                fulfill(dir);
            });
        });
    });
}

/**
 * Checks whether the passed `filePath` identifies an existing file.
 * @param {string} filePath - the name of the file whose existence should be checked
 * @returns {Promise} a Promise that resolves to boolean indicating if the file exists 
 */
function fileExists (filePath) {
    return new Promise((fulfill, reject) => {
        // try to open the file to check if it exists
        try {
            fs.open(filePath, 'r', (err, fd) => {
                if (err) {
                    // if error, assume file doesn't exist and return false
                    fulfill(false);
                } else {
                    // if the file does exist, close it and return true
                    fs.close(fd, () => {
                        fulfill(true);
                    });
                }
            });
        } catch (err) {
            // istanbul ignore next
            reject('Unexpected error reading file');
        }
    });
}

/**
 * Finds all files in a directory and its subdirectories.
 * @param {string} dir - the path to the directory that is to be searched
 * @return {Promise} a Promise that resolves to an an array of found file names, or null if directory is empty 
 * (note: the file names are relative paths based in the initial passed directory)
 */
function findFiles (dir) {
    let retFiles = [];
    let counter = 0;
    return new Promise((fulfill, reject) => {
        // return null if no directory passed
        if (!dir) {
            fulfill(null);
        }
        // get dir info
        fs.lstat(dir, (err, stats) => {
            // if it's a file, return it
            if (stats.isFile()) {
                fulfill([dir]);
            } else if (stats.isDirectory()) {
                // if it's a directory, read the contents
                fs.readdir(dir, (err, files) => {
                    if (files != null && files.length > 0) {
                        // get info of all files inside the dir
                        for (let file of files) {
                            fs.lstat(path.join(dir, file), (err, stats) => {
                                // if file, add to retFiles
                                if (stats.isFile()) {
                                    retFiles.push(file);
                                    counter++;
                                    if (counter == files.length) {
                                        fulfill(retFiles);
                                    }
                                } else if (stats.isDirectory()) {
                                    // if directory, then recursion
                                    findFiles(path.join(dir, file)).then((foundFiles) => {
                                        if (foundFiles) {
                                            // add dir to each file found
                                            let dirFoundFiles = foundFiles.map((f) => {
                                                return path.join(file, f);
                                            });
                                            retFiles = retFiles.concat(dirFoundFiles);
                                        }
                                        counter++;
                                        // if all files have been processed, return
                                        if (counter == files.length) {
                                            fulfill(retFiles);
                                        }

                                    });
                                }
                            });
                        }
                    } else {
                        // if directory is empty, return null
                        fulfill(null);
                    }
                });
            }
        });
    });
}

/**
 * Extracts and returns the contents of a file identified by `filePath`.
 * @param {string} filePath - the name of the file to get content of
 * @returns {Promise} a Promise that resolves to the contents of the file identified by `filePath`
 */
function getFileContents (filePath) {
    return new Promise((fulfill, reject) => {
        // read the file with utf-8 encoding
        fs.readFile(filePath, {
            encoding: 'utf-8'
        }, (err, data) => {
            if (err) {
                reject(err);
            }
            try {
                // return the data read from the file
                fulfill(data);
            } catch (ex) {
                // istanbul ignore next
                reject(ex);
            }
        });
    });
}

/**
 * Returns the type of file based on the file extension.
 * @param {string} filePath - the path of the file
 * @returns {string} the type of file as determined by `filePath` filename extension
 */
function getFileType (filePath) {
    let ext = path.extname(filePath);
    let ret = '';
    if (ext === '.htm' || ext === '.html') {
        ret = 'html';
    } else if (ext === '.css') {
        ret = 'css';
    }
    return ret;
}

/**
 * Checks if the archive passed can be validated.
 * @param {string} archivePath - the path of the archive to be validated
 * @returns {Promise} a Promise that resolves to an object showing if the archive is ok for validation
 */
function isArchiveOk (archivePath) {
    // ensure that an archive was passed
    if (!archivePath) {
        return Promise.resolve({
            ok: false,
            message: 'No archive passed to function'
        });
    }

    // check if the archive file extension is valid
    if (!checkArchiveExtension(archivePath)) {
        return Promise.resolve({
            ok: false,
            message: 'The passed archive ends in an unaccepted extension'
        });
    }

    // check if the archive exists
    return fileExists(archivePath).then((exists) => {
        return new Promise((fulfill, reject) => {
            let msg = exists ? 'ok' : 'The archive does not exist';
            fulfill({
                ok: exists,
                message: msg
            });
        });
    });
}

/**
 * Checks if the file passed can be validated.
 * @param {string} filePath - the path of the file to be validated
 * @returns {Promise} a Promise that resolves to an object showing if the 
 * file is ok for validation
 */
function isFileOk (filePath) {
    // ensure that a file was passed to this function
    if (!filePath) {
        return Promise.resolve({
            ok: false,
            message: 'No file passed to function'
        });
    }

    // ensure that file has an accepted extension
    if (!checkFileExtension(filePath)) {
        return Promise.resolve({
            ok: false,
            message: 'The passed file ends in an unaccepted extension'
        });
    }

    // check that the file exists
    return fileExists(filePath).then((exists) => {
        return new Promise((fulfill, reject) => {
            let msg = exists ? 'ok' : 'The file does not exist';
            fulfill({
                ok: exists,
                message: msg
            });
        });
    });
}

module.exports = {
    checkArchiveExtension: checkArchiveExtension,
    checkFileExtension: checkFileExtension,
    deleteDirectory: deleteDirectory,
    deleteFile: deleteFile,
    deleteFileList: deleteFileList,
    extractArchive: extractArchive,
    fileExists: fileExists,
    findFiles: findFiles,
    getFileContents: getFileContents,
    getFileType: getFileType,
    isArchiveOk: isArchiveOk,
    isFileOk: isFileOk
};
