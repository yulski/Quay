/**
 * This module normalizes responses from the various APIs, so that they all take the same JSON format.
 * @module normalize
 */

const Message = require('../models').Message;

/**
 * Normalizes the response from a validation API and returns the normalized response.
 * @param {Object} resp - the response returned from a validation service
 * @param {string} service - the validation service that the response was returned from (e.g. 'html' or 'css')
 * @returns {Object} a normalized version of `resp`
 */
function normalize(resp, service) {
    // create the initial form for the response 
    let normalizedResponse = {
        messages: []
    };
    // handle HTML API response normalization
    if (service === 'html') {
        let currMessage;
        // normalize each message in the "messages" array and add them to the 
        // normalized response
        for (let msg of resp.messages) {
            currMessage = normalizeHtmlResponse(msg);
            normalizedResponse.messages.push(currMessage);
        }
    }
    // handle CSS API response normalization
    else if (service === 'css') {
        let currMessage;
        // normalize each error and add to normalized response
        for (let err of resp.errors) {
            currMessage = normalizeCssResponse(err, 'error');
            normalizedResponse.messages.push(currMessage);
        }
        // normalize each warning and add to normalized response
        for (let warn of resp.warnings) {
            currMessage = normalizeCssResponse(warn, 'warning');
            normalizedResponse.messages.push(currMessage);
        }
    } else if (service === 'heck') {
        let currMessage;
        for (let msg of resp) {
            currMessage = normalizeHeckResponse(msg);
            normalizedResponse.messages.push(currMessage);
        }
    }
    return normalizedResponse;
}

/**
 * Normalizes response from the W3C HTML validation service.
 * @param {Object} message - a message object from the "messages" array in the response
 * @returns {Object} normalized response
 */
// istanbul ignore next
function normalizeHtmlResponse(message) {
    let respMessage = Message.build({
        type: message.type,
        firstLine: +message.firstLine || +message.lastLine || 0,
        lastLine: +message.lastLine || 0,
        firstColumn: +message.firstColumn || 0,
        lastColumn: +message.lastColumn || 0,
        message: message.message,
        extract: message.extract
    });
    return respMessage;
}

/**
 * Normalizes the response from the W3C CSS validation API.
 * @param {Object} message - a message object from the "errors" or the "warnings" array in the response
 * @param {string} type - the type of message this is (either 'warning' or 'error')
 * @returns {Object} normalized response
 */
// istanbul ignore next
function normalizeCssResponse(message, type) {
    let respMessage = Message.build({
        type: type,
        firstLine: +message.line,
        lastLine: +message.line,
        message: message.message,
        otherInfo: message.type
    });
    return respMessage;
}

/**
 * Normalizes the response returned from the Heck module.
 * @param {Object} message - the Heck message
 * @returns {Object} normalized message
 */
function normalizeHeckResponse(message) {
    let respMessage = Message.build({
        type: message.type,
        firstLine: +message.startLine || +message.endLine,
        lastLine: +message.endLine,
        message: message.message,
    });
    return respMessage;
}

module.exports = normalize;
