/**
 * Validation module which performs validation of passed file and returns the resutls.
 * @module validation
 */

const path = require('path');
const unirest = require('unirest');
const w3cCss = require('w3c-css');

const fh = require('./file-handling');
const normalize = require('./normalize');

/**
 * Functions for making requests to different validation APIs, indexed by the language. All functions accept 
 * the text to be sent in the request and a callback that will be called with the APIs response.
 */
const requestFuncs = {
    // function that makes request to W3C CSS validation API
    css: (text, callback) => {
        w3cCss.validate({text: text}, (err, data) => {
            callback(data);
        });
    },
    // request to W3C HTML validation API
    html: (text, callback) => {
        unirest.post('http://validator.w3.org/nu/?out=json')
            .headers({'Content-Type': 'text/html', 'charset': 'utf-8', 'user-agent': 'validation.js'})
            .send(text)
            .end(res => {
                callback(res.body);
            });
    }
};

/**
 * Validates the content of a file identified by `fileName` and returns a Promise that resolves to the 
 * validation service response body.
 * @param {string} filePath - the name of the file to be validated
 * @param {string=} lang - the laguage of the file being validated (if not passed, file extension will be used to 
 * determine the language)
 * @returns {Promise} a Promise that resolves to the body of the response from the validation service 
 */
function validateFile (filePath, lang) {
    // throw an error if no file name was passed
    if (!filePath) {
        return Promise.reject('No file path passed to validation.js');
    }

    // if lang not passed, get it based on file extension
    if(!lang) {
        lang = fh.getFileType(filePath);
    }

    // return a promise with the response body
    return new Promise((fulfill, reject) => {
        // get the file contents and then validate
        fh.getFileContents(filePath).then((contents) => {
            validateText(contents, lang).then((res) => {
                // add file name to each message
                for(let message of res.messages) {
                    message.file = path.basename(filePath);
                }
                fulfill(res);
            });
        });
    });
}

/**
 * Validates a list of files.
 * @param fileList {string[]} - an array paths of the files to be validated
 * @returns {Promise} a Promise that resolves to an array of responses from the validation service
 */
function validateFileList (fileList) {
    // ensure a file list was passed
    if(!fileList) {
        return Promise.reject('No file list passed to validation.js');
    }

    // array of promise responses from file validation
    let promises = [];

    // validate each file
    for(let file of fileList) {
        promises.push(validateFile(file));
    }

    // resolve all promises
    return Promise.all(promises);
}

/**
 * Validates the text in the string `text` and returns a Promise which resolves to the validation service 
 * response body, or an empty object if the passed `text` string was empty.
 * @param {string} text - the text to be validated
 * @param {string} lang - the language to perform validation for
 * @returns {Promise} a Promise that resolves to the body of the response from the validation service
 */
function validateText (text, lang) {
    // return an empty object if the text string is empty
    if (text.length === 0) {
        return Promise.resolve({});
    }

    // returns a Promise
    return new Promise((fulfill, reject) => {
        // post the request to the validation service
        requestFuncs[lang](text, (res) => {
            // normalize the response
            let normalized = normalize(res, lang);
            // istanbul ignore next
            try {
                fulfill(normalized);
            } catch(ex) {
                reject(ex);
            }
        });
    });
}

module.exports = {
    validateFile: validateFile,
    validateFileList: validateFileList,
    validateText: validateText
};
